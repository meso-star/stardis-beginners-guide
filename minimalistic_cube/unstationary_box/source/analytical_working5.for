      subroutine analytical(dim,Nprobe,probe_positions,
     &     Nterms_fs,Nterms_pq,temp_file,green_file)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
c     
c     Purpose: perform analytical computation
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Nterms_fs: number of terms in the Fourier series expansion
c       + Nterms_pq: number of terms in the sums over p and q
c       + temp_file: name of the file for recording temperatures
c       + green_file: name of the file for recording green functions
c     
c     Output:
c       + the "results_file" file
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      integer Nterms_fs,Nterms_pq
      character*(Nchar_mx) temp_file,green_file
c     temp
      integer iprobe,i,j,m,n,o,p,q
      double precision a,b,c
      double precision Tb(1:2*Nvec_mx)
      double precision itg(1:10)
      double precision Lx2,Ly2,Lz2,Lx,Ly,Lz
      double precision alpha,beta,gamma,eta,zeta
      double precision beta2,gamma2,eta2
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision T
      double precision z1,z2,z3
      logical is_nan,is_inf
      integer p1,q1
      logical m_is_even,n_is_even,o_is_even
      double precision G(0:6),Gs(0:6),Gi(0:6),Gtmp(1:6)
      double precision Fxyzt
      double precision pq,pi2
      double precision tmp1
c     functions
      logical is_even
c     label
      character*(Nchar_mx) label
      label='subroutine analytical'

      write(*,*) 'Performing analytical computation...'
c     ==================================================================================
c     STEADY-STATE solution
c     ==================================================================================
      alpha=lambda_s/(rho_s*Cp_s)
      a=box(1,2)-box(1,1)
      b=box(2,2)-box(2,1)
      c=box(3,2)-box(3,1)
      Tb(1)=Tboundary(1,1)
      Tb(2)=Tboundary(1,2)
      Tb(3)=Tboundary(2,1)
      Tb(4)=Tboundary(2,2)
      Tb(5)=Tboundary(3,1)
      Tb(6)=Tboundary(3,2)
      do i=0,6
         Gs(i)=0.0D+0
      enddo                     ! i
      pi2=pi**2.0D+0
c      
      do iprobe=1,Nprobe
         do j=1,dim
            probe_x(j)=probe_positions(iprobe,j)
         enddo                  ! j
         probe_time=probe_positions(iprobe,dim+1)
c     Tx: temperature due to (T1,T2)
         do i=1,6
            Gtmp(i)=0.0D+0
         enddo                  ! i
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Lx2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Lx=dsqrt(Lx2)
               tmp1=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lx*a))
               Gtmp(1)=Gtmp(1)+dsinh(Lx*(a-probe_x(1)))*tmp1
               Gtmp(2)=Gtmp(2)+dsinh(Lx*probe_x(1))*tmp1
            enddo               ! q
         enddo                  ! p
c     Ty: temperature due to (T3,T4)
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Ly2=pi2*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Ly=dsqrt(Ly2)
               tmp1=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Ly*b))
               Gtmp(3)=Gtmp(3)+dsinh(Ly*(b-probe_x(2)))*tmp1
               Gtmp(4)=Gtmp(4)+dsinh(Ly*probe_x(2))*tmp1
            enddo               ! q
         enddo                  ! p
c     Tz: temperature due to (T5,T6)
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Lz2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
               Lz=dsqrt(Lz2)
               tmp1=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lz*c))
               Gtmp(5)=Gtmp(5)+dsinh(Lz*(c-probe_x(3)))*tmp1
               Gtmp(6)=Gtmp(6)+dsinh(Lz*probe_x(3))*tmp1
            enddo               ! q
         enddo                  ! p
         do i=1,6
            Gs(i)=Gs(i)+Gtmp(i)*16.0D+0/pi2
         enddo                  ! i
c     Ts: sum of Tx, Ty and Tz ; steady-state solution for (probe_x, probe_t)
c     ==================================================================================
c     TRANSIENT solution
c     ==================================================================================
         do i=0,6
            Gi(i)=0.0D+0
         enddo                  ! i
         do m=1,Nterms_fs
            beta=m*pi/a
            beta2=beta**2.0D+0
            m_is_even=is_even(m)
            do n=1,Nterms_fs
               gamma=n*pi/b
               gamma2=gamma**2.0D+0
               n_is_even=is_even(n)
               do o=1,Nterms_fs
                  eta=o*pi/c
                  eta2=eta**2.0D+0
                  o_is_even=is_even(o)
c
                  zeta=alpha*(beta2+gamma2+eta2)
                  do i=1,6
                     Gtmp(i)=0.0D+0
                  enddo         ! i
                  do p=0,Nterms_pq
                     do q=0,Nterms_pq
                        Lx2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Ly2=pi2*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Lz2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
 2                     pq=(2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
c     itg(1)
                        if (2*q-o+1.eq.0) then
                           itg(1)=-c/2.0D+0
                        else
                           itg(1)=0.0D+0
                        endif
c     itg(2)
                        itg(2)=eta/(eta2+Lz2)
c     itg(4)
                        if (2*p-n+1.eq.0) then
                           itg(4)=-b/2.0D+0
                        else
                           itg(4)=0.0D+0
                        endif
c     itg(5)
                        itg(5)=gamma/(gamma2+Ly2)
c     itg(7)
                        itg(7)=beta/(beta2+Lx2)
c     itg(9)
                        if (2*p-m+1.eq.0) then
                           itg(9)=-a/2.0D+0
                        else
                           itg(9)=0.0D+0
                        endif
c     itg(10)
                        if (2*q-m+1.eq.0) then
                           itg(10)=-a/2.0D+0
                        else
                           itg(10)=0.0D+0
                        endif
c     
                        if ((2*q-o+1.eq.0).and.(2*p-n+1.eq.0)) then
                           z1=itg(1)*itg(4)*itg(7)
                           Gtmp(1)=Gtmp(1)+z1/pq
                           Gtmp(2)=Gtmp(2)+(z1*(-1.0D+0)**dble(m+1))/pq
                        endif
                        if ((2*q-o+1.eq.0).and.(2*p-m+1.eq.0)) then
                           z2=itg(1)*itg(9)*itg(5)
                           Gtmp(3)=Gtmp(3)+z2/pq
                           Gtmp(4)=Gtmp(4)+(z2*(-1.0D+0)**dble(n+1))/pq
                        endif
                        if ((2*p-n+1.eq.0).and.(2*q-m+1.eq.0)) then
                           z3=itg(4)*itg(10)*itg(2)
                           Gtmp(5)=Gtmp(5)+z3/pq
                           Gtmp(6)=Gtmp(6)+(z3*(-1.0D+0)**dble(o+1))/pq
                        endif
                     enddo      ! q
                  enddo         ! p
                  Fxyzt=dsin(beta*probe_x(1))
     &                 *dsin(gamma*probe_x(2))
     &                 *dsin(eta*probe_x(3))
     &                 *dexp(-zeta*probe_time)
                  if ((.not.m_is_even).and.
     &                 (.not.n_is_even).and.
     &                 (.not.o_is_even)) then
                     Gi(0)=Gi(0)+8.0D+0*Fxyzt/(beta*gamma*eta)
                  endif
                  do i=1,6
                     Gi(i)=Gi(i)+Gtmp(i)*Fxyzt
                  enddo         ! i
               enddo            ! o
            enddo               ! n
         enddo                  ! m
c     
         Gi(0)=Gi(0)*8.0D+0/(a*b*c)
         do i=1,6
            Gi(i)=-Gi(i)*128.0D+0/(a*b*c*pi2)
         enddo                  ! i
c     
c     The requested temperature is T=Ts+T2
         do i=0,6
            G(i)=Gs(i)+Gi(i)
         enddo                  ! i
c     
         T=G(0)*Tinit
         do i=1,6
            T=T+G(i)*Tb(i)
         enddo                  ! i
c     
         open(11,file=trim(temp_file),access='append')
         write(11,*) (probe_x(i),i=1,dim),probe_time,T
         close(11)
         open(12,file=trim(green_file),access='append')
         write(12,*) (probe_x(i),i=1,dim),probe_time,(G(i),i=0,6)
         close(12)
c     
      enddo                     ! iprobe
      write(*,*) 'Files have been generated:'
      write(*,*) trim(temp_file)
      write(*,*) trim(green_file)

      return
      end
