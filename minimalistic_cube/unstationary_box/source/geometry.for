      subroutine distance_Nd(dim,p1,p2,d)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the distance between 2 points defined in N dimensions
c     
c     Input:
c       + dim: dimension of space
c       + p1: first point
c       + p2: second point
c     
c     Output:
c       + d: distance
c
c     I/O
      integer dim
      double precision p1(1:Nvec_mx)
      double precision p2(1:Nvec_mx)
      double precision d
c     temp
      double precision u(1:Nvec_mx)
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine distance_Nd'

      call substract_vectors(dim,p2,p1,u)
      call vector_length(dim,u,d)
      
      return
      end
      


      subroutine spher2cart(r,theta,phi,x,y,z)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert (r,theta,phi) spherical coordinates
c     into (x,y,z) cartesian coordinates
c
c     Inputs:
c       + r: radius (m), or altitude relative to the center of the planet
c       + theta: latitude (rad) [-pi/2,pi/2]
c       + phi: longitude rad) [0:2*pi]
c
c     Outputs:
c       + x (m)
c       + y (m)
c       + z (m)
c
c     I/O
      double precision r,theta,phi
      double precision x,y,z
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine spher2cart'

      x=r*dcos(theta)*dcos(phi)
      y=r*dcos(theta)*dsin(phi)
      z=r*dsin(theta)
      
      return
      end


      
      subroutine distance_cart(x1,y1,z1,x2,y2,z2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their cartesian coordinates
c
c     inputs
      double precision x1,y1,z1
      double precision x2,y2,z2
c     outputs
      double precision d
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine distance_cart'

      d=dsqrt((x2-x1)**2.0D+0+(y2-y1)**2.0D+0+(z2-z1)**2.0D+0)

      return
      end
