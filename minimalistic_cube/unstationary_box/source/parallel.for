      subroutine send_global_data_mpibcast(np,dim)
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to send common data to all processes
c     
c     Input:
c       + np: total number of processes
c       + dim: dimension of space
c     
c     Output: all common data sent
c     
c     MPICH
      integer ierr
      integer stat(MPI_STATUS_SIZE)
c     temp
      integer np,dim
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine send_global_data_mpibcast'

      call MPI_BCAST(dim,1,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Nevent,1,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(seeds,Nproc_mx,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(rho_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Cp_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(lambda_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(bfactor,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(sfactor,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(emin,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Tinit,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Tboundary,2*Nvec_mx,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(box,2*Nvec_mx,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)

      return
      end


      
      subroutine receive_global_data_mpibcast(np,dim)
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to receive common data
c     
c     Input:
c       + np: total number of processes
c       + dim: dimension of space
c     
c     Output: all common data received
c     
c     
c     MPICH
      integer ierr
      integer stat(MPI_STATUS_SIZE)
c     temp
      integer np,dim
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine receive_global_data_mpibcast'

      call MPI_BCAST(dim,1,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Nevent,1,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(seeds,Nproc_mx,MPI_INTEGER
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(rho_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Cp_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(lambda_s,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(bfactor,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(sfactor,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(emin,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Tinit,1,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Tboundary,2*Nvec_mx,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(box,2*Nvec_mx,MPI_DOUBLE_PRECISION
     &     ,0,MPI_COMM_WORLD,ierr)

      return
      end
      
