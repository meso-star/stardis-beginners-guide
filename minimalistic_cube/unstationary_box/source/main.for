      program main
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
      include 'formats.inc'
c     
c     Purpose: to compute a temperature using a statistical algorithm
c
c     MPICH
      integer ierr,errorcode,np,pindex,proc
      integer stat(MPI_STATUS_SIZE)
      logical eoc,fpf
      logical free(1:Nproc_mx)
      logical stopped(1:Nproc_mx)
      integer code0,codep
c     Variables
      integer dim,seed
      character*(Nchar_mx) data_file
      character*(Nchar_mx) options_file
      character*(Nchar_mx) temp_file
      character*(Nchar_mx) green_file
      character*(Nchar_mx) results_file
      integer Nterms_fs,Nterms_pq
      logical mc
      double precision probe_time,t0,init_time
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      integer Nstf_x,Nstf_init
      double precision stf_positions(1:Np_mx,1:Nvec_mx+1)
      integer type
      double precision probe_x(1:Nvec_mx)
      double precision x0(1:Nvec_mx)
      double precision init_x(1:Nvec_mx)
      integer boundary_index
      logical debug
      double precision Ganalytical(1:Np_mx,0:6)
      double precision Tanalytical(1:Np_mx)
      double precision avg,variance,std_dev
      double precision T,contrib,sum,sum2
      double precision Ts,delta_Ts
      double precision r
      integer itime
c     analytical results
c     progress display
      integer ndone,Nevent_tot
      double precision fdone
      character*(Nchar_mx) str
      character*(Nchar_mx) fmt
      integer err
c     temp
      integer len,i,j,iprobe,event,nsp
      integer it,ix
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='program main'

c     MPICH
      call MPI_INIT(ierr)
      if (ierr.NE.MPI_SUCCESS) THEN
         call error(label)
         write(*,*) 'MPI could not be initialized'
         call MPI_ABORT(MPI_COMM_WORLD,errorcode,ierr)
      endif
c     Get 'np', the number of processes
      call MPI_COMM_SIZE(MPI_COMM_WORLD,np,ierr)
      if (np.lt.2) then
         call error(label)
         write(*,*) 'Number of processes is:',np
         write(*,*) 'and it should be greater than 1'
         stop
      endif
      if (np.gt.Nproc_mx) then
         call error(label)
         write(*,*) 'Number of processes is:',np
         write(*,*) 'while Nproc_mx=',Nproc_mx
         stop
      endif
c     Get 'pindex', the index of the current process
      call MPI_COMM_RANK(MPI_COMM_WORLD,pindex,ierr)
c     MPICH
c     
      if (pindex.eq.0) then
         dim=3
         len=6
         call num2str(len,str,err)
         if (err.eq.0) then
            fmt='(i'//trim(str)//',a)'
         endif
c     Initialization:
c     + read random seed
         call read_seed(seed)
c     + Initialization of the random number generator
         call zufalli(seed)
c     + read input data (configuration, external conditions...)
         data_file='./data.in'
         call read_data(data_file,dim,
     &        box,rho_s,Cp_s,lambda_s,
     &        Tinit,Tboundary,
     &        bfactor,sfactor,Nterms_fs,Nterms_pq,mc,
     &        Nevent,Nprobe,probe_positions)
         call min_box_dimension(dim)
c     + remove previous result files
         temp_file='./results/analytical_probe_temperatures.out' ! analytical temperatures
         green_file='./results/analytical_green.out' ! analytical green functions
         results_file='./results/probe_temperatures.out' ! MC temperatures
         call init(temp_file,green_file,results_file)
c     + perform analytical computation
         call analytical(dim,Nprobe,probe_positions,
     &        Nterms_fs,Nterms_pq,temp_file,green_file,
     &        Ganalytical,Tanalytical)
c     
c     =================================================================================================
         if (mc) then
            ndone=0
            Nevent_tot=Nevent*Nprobe
            write(*,*) 'Performing MC evaluation...'
            write(*,"(a)",advance='no') 'Done:     0 %'
c     
            debug=.false.
            do iprobe=1,Nprobe
               do j=1,dim
                  probe_x(j)=probe_positions(iprobe,j)
               enddo            ! j
               probe_time=probe_positions(iprobe,dim+1)
c     Temperature in the solid: type=2
               type=2
c     initialization of sums
               sum=0.0D+0
               sum2=0.0D+0
c     sending common data to all processes
               code0=1
               do proc=1,np-1
                  call MPI_SEND(code0,1,MPI_INTEGER
     &                 ,proc,1,MPI_COMM_WORLD,ierr)
               enddo            ! proc
               do proc=1,np-1
                  call random_gen(r)
                  seeds(proc)=int(10000*r)
               enddo            ! proc
               call send_global_data_mpibcast(np,dim)
c     MC loop
               do proc=1,np-1
                  free(proc)=.true.
                  stopped(proc)=.false.
               enddo
               eoc=.false.
               event=0
               do while (.not.eoc)
 442              continue
                  call find_free_process(free,np-1,fpf,proc)
                  if (fpf) then ! a free child process found
                     event=event+1
                     if (event.le.Nevent) then
                        code0=2
                        call MPI_SEND(code0,1,MPI_INTEGER
     &                       ,proc,1,MPI_COMM_WORLD,ierr)
                        call MPI_SEND(debug,1,MPI_LOGICAL
     &                       ,proc,1,MPI_COMM_WORLD,ierr)
                        call MPI_SEND(dim,1,MPI_INTEGER
     &                       ,proc,1,MPI_COMM_WORLD,ierr)
                        call MPI_SEND(type,1,MPI_INTEGER
     &                       ,proc,1,MPI_COMM_WORLD,ierr)
                        call MPI_SEND(boundary_index,1,MPI_INTEGER
     &                       ,proc,1,MPI_COMM_WORLD,ierr)
                        call MPI_SEND(probe_time,1,
     &                       MPI_DOUBLE_PRECISION,proc,1,
     &                       MPI_COMM_WORLD,ierr)
                        call MPI_SEND(probe_x,dim,
     &                       MPI_DOUBLE_PRECISION,proc,1,
     &                       MPI_COMM_WORLD,ierr)
                        free(proc)=.false. ! child process index 'proc' is busy
                     else       ! chunk>nchunks or Nkrun(chunk)=0
                        free(proc)=.false. ! child process index 'proc' is busy
                        stopped(proc)=.true. ! means child process index 'proc' has been stopped
                     endif
                     goto 442
                  else          ! no free child process found
                     call stopped_processes(stopped,np-1,nsp)
                     if (nsp.eq.np-1) then
                        eoc=.true.
                        goto 112
                     endif
c     wait for results
                     call MPI_RECV(proc,1,MPI_INTEGER
     &                    ,MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,stat,ierr)
                     call MPI_RECV(contrib,1,MPI_DOUBLE_PRECISION
     &                    ,proc,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,stat,ierr)
c     print status
                     ndone=ndone+1
                     fdone=dble(ndone)/dble(Nevent_tot)*1.0D+2
                     if (fdone.eq.dble(floor(fdone))) then
                        do j=1,len+2
                           write(*,"(a)",advance='no') "\b"
                        enddo   ! j
                        write(*,trim(fmt),advance='no')
     &                       floor(fdone),' %'
                     endif
c     update sums
                     sum=sum+contrib
                     sum2=sum2+contrib**2.0D+0
c     release the process
                     free(proc)=.true. ! child process index 'proc' is free again
c     
                  endif         ! fpf or not
 112              continue
               enddo            ! while eoc=.false.
c     
c     get the average and standard deviation
               call statistics(Nevent,sum,sum2,avg,variance,std_dev)
               Ts=avg
               delta_Ts=std_dev
c     --------------------------------------------------------------------------------------
c     Append results records file:
               open(11,file=trim(results_file),access='append')
               write(11,*) (probe_x(i),i=1,dim),probe_time,Ts,delta_Ts
               close(11)
c     --------------------------------------------------------------------------------------
            enddo               ! iprobe
            write(*,*)
            write(*,*) 'File was generated:'
            write(*,*) trim(results_file)
c     
c     
c     Before quitting: generate a new random seed
c            call write_seed()
         endif                  ! mc
c     =================================================================================================
c     stopping all child processes
         do proc=1,np-1
            code0=0
            call MPI_SEND(code0,1,MPI_INTEGER
     &           ,proc,1,MPI_COMM_WORLD,ierr)
         enddo
c     
         else                   ! every other process
c     
 333        continue
c     wait for "codep" from root process
            call MPI_RECV(codep,1,MPI_INTEGER
     &           ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
c     codep=0 means exit
            if (codep.eq.0) then
               goto 666
c     codep=1 means common data reception
            else if (codep.eq.1) then
               call receive_global_data_mpibcast(np,dim)
c     initialization of the random number generator
            call zufalli(seeds(pindex))
c     codep=2 means data reception and computation
            else if (codep.eq.2) then
               call MPI_RECV(debug,1,MPI_LOGICAL
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(dim,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(type,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(boundary_index,1,MPI_INTEGER
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(init_time,1,MPI_DOUBLE_PRECISION
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
               call MPI_RECV(init_x,dim,MPI_DOUBLE_PRECISION
     &              ,0,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
c     computation
               call get_temperature(debug,dim,type,boundary_index,
     &              init_x,init_time,T)
c     send results to root process
               call MPI_SEND(pindex,1,MPI_INTEGER
     &              ,0,1,MPI_COMM_WORLD,ierr)
               call MPI_SEND(T,1,MPI_DOUBLE_PRECISION
     &              ,0,1,MPI_COMM_WORLD,ierr)
            endif               ! codep
c     
c     wait for a new execution code
            goto 333
c     
 666        continue
c     
      endif                     ! pindex
c     MPICH
      call MPI_FINALIZE(ierr)
c     MPICH

      end
      
