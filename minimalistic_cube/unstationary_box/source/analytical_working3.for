      subroutine analytical(dim,Nprobe,probe_positions,
     &     Nterms_fs,Nterms_pq,results_file)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
c     
c     Purpose: perform analytical computation
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Nterms_fs: number of terms in the Fourier series expansion
c       + Nterms_pq: number of terms in the sums over p and q
c       + results_file: name of the results file to generate
c     
c     Output:
c       + the "results_file" file
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      integer Nterms_fs,Nterms_pq
      character*(Nchar_mx) results_file
c     temp
      integer iprobe,i,j,m,n,o,p,q
      double precision a,b,c
      double precision Tb(1:2*Nvec_mx)
      double precision itg(1:10)
      double precision Lx2,Ly2,Lz2
      double precision alpha,beta,gamma,eta
      double precision Cmno
      double precision sum
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision Tx,Ty,Tz,Tpq
      double precision Ts,T2,T
      double precision L,L2
      double precision term1,term2
      logical is_nan,is_inf
c     label
      character*(Nchar_mx) label
      label='subroutine analytical'

      write(*,*) 'Performing analytical computation...'
c     ==================================================================================
c     STEADY-STATE solution
c     ==================================================================================
      alpha=lambda_s/(rho_s*Cp_s)
      a=box(1,2)-box(1,1)
      b=box(2,2)-box(2,1)
      c=box(3,2)-box(3,1)
      Tb(1)=Tboundary(1,1)
      Tb(2)=Tboundary(1,2)
      Tb(3)=Tboundary(2,1)
      Tb(4)=Tboundary(2,2)
      Tb(5)=Tboundary(3,1)
      Tb(6)=Tboundary(3,2)
      do iprobe=1,Nprobe
         do j=1,dim
            probe_x(j)=probe_positions(iprobe,j)
         enddo                  ! j
         probe_time=probe_positions(iprobe,dim+1)
c     Tx: temperature due to (T1,T2)
         sum=0.0D+0
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               L2=((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0
               L=pi*dsqrt(L2)
               sum=sum+((Tb(1)*dsinh(L*(a-probe_x(1)))
     &              +Tb(2)*dsinh(L*probe_x(1)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(L*a))
c     Debug
               if (dsinh(L*(a-probe_x(1))).gt.1.0D+300) then
                  write(*,*) p,q,L,dsinh(L*(a-probe_x(1)))
                  stop
               endif
c     Debug
            enddo               ! q
         enddo                  ! p
         Tx=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Tx,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Tx=',Tx
            write(*,*) 'L2=',L2
            write(*,*) dsinh(L*(a-probe_x(1)))
            write(*,*) dsinh(L*probe_x(1))
            stop
         endif
c     Debug
c     Ty: temperature due to (T3,T4)
         sum=0.0D+0
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               L2=((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0
               L=pi*dsqrt(L2)
               sum=sum+((Tb(3)*dsinh(L*(b-probe_x(2)))
     &              +Tb(4)*dsinh(L*probe_x(2)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(L*b))
            enddo               ! q
         enddo                  ! p
         Ty=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Ty,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Ty=',Ty
            stop
         endif
c     Debug
c     Tz: temperature due to (T5,T6)
         sum=0.0D+0
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               L2=((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/a)**2.0D+0
               L=pi*dsqrt(L2)
               sum=sum+((Tb(5)*dsinh(L*(c-probe_x(3)))
     &              +Tb(6)*dsinh(L*probe_x(3)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(L*c))
            enddo               ! q
         enddo                  ! p
         Tz=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Tz,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Tz',Tz
            stop
         endif
c     Debug
c     Ts: sum of Tx, Ty and Tz ; steady-state solution for (probe_x, probe_t)
         Ts=Tx+Ty+Tz
c     ==================================================================================
c     TRANSIENT solution
c     ==================================================================================
         T2=0.0D+0
         do m=1,Nterms_fs
            beta=m*pi/a
            do n=1,Nterms_fs
               gamma=n*pi/b
               do o=1,Nterms_fs
                  eta=o*pi/c
                  sum=0.0D+0
                  do p=0,Nterms_pq
                     do q=0,Nterms_pq
                        Lx2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
c                        Lx=pi*dsqrt(L2)
                        Ly2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
c                        Ly=pi*dsqrt(L2)
                        Lz2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
c                        Lz=pi*dsqrt(L2)
c     itg(1)
                        if (2*q-o+1.eq.0) then
                           itg(1)=-c/2.0D+0
                        else
                           itg(1)=0.0D+0
                        endif
c     itg(2)
                        itg(2)=eta/
     &                       (eta**2.0D+0+Lz2)
c     itg(3)
                        itg(3)=itg(2)*(-1.0D+0)**dble(o+1)
c     itg(4)
                        if (2*p-n+1.eq.0) then
                           itg(4)=-b/2.0D+0
                        else
                           itg(4)=0.0D+0
                        endif
c     itg(5)
                        itg(5)=gamma/
     &                       (gamma**2.0D+0+Ly2)
c     itg(6)
                        itg(6)=itg(5)*(-1.0D+0)**dble(n+1)
c     itg(7)
                        itg(7)=beta/
     &                       (beta**2.0D+0+Lx2)
c     itg(8)
                        itg(8)=itg(7)*(-1.0D+0)**dble(m+1)
c     itg(9)
                        if (2*p-m+1.eq.0) then
                           itg(9)=-a/2.0D+0
                        else
                           itg(9)=0.0D+0
                        endif
c     itg(10)
                        if (2*q-m+1.eq.0) then
                           itg(10)=-a/2.0D+0
                        else
                           itg(10)=0.0D+0
                        endif
                        
                        Tpq=(
     &                       itg(1)*itg(4)*(Tb(1)*itg(7)+Tb(2)*itg(8))
     &                       +itg(1)*itg(9)*(Tb(3)*itg(5)+Tb(4)*itg(6))
     &                       +itg(4)*itg(10)*(Tb(5)*itg(2)+Tb(6)*itg(3))
     &                       )
     &                       /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                        sum=sum+Tpq
                     enddo      ! q
                  enddo         ! p
                  Cmno=8.0D+0/(a*b*c)*(
     &                 Tinit*(1.0D+0+(-1.0D+0)**(dble(m)+1.0D+0))
     &                 *(1.0D+0+(-1.0D+0)**(dble(n)+1.0D+0))
     &                 *(1.0D+0+(-1.0D+0)**(dble(o)+1.0D+0))
     &                 /(beta*gamma*eta)
     &                 -sum*16.0D+0/(pi**2.0D+0))
                  T2=T2
     &                 +Cmno
     &                 *dsin(beta*probe_x(1))
     &                 *dsin(gamma*probe_x(2))
     &                 *dsin(eta*probe_x(3))
     &                 *dexp(-alpha*
     &                 (beta**2.0D+0
     &                 +gamma**2.D0+0
     &                 +eta**2.0D+0)
     &                 *probe_time)
c     Debug
                  call test_nan(T2,is_nan)
                  if (is_nan) then
                     call error(label)
                     write(*,*) 'T2=',T2
                     write(*,*) 'm=',m,' n=',n,' o=',o
                     write(*,*) 'Cmno=',Cmno
                     write(*,*) dsin(beta*probe_x(1))
                     write(*,*) dsin(gamma*probe_x(2))
                     write(*,*) dsin(eta*probe_x(3))
                     write(*,*) dexp(-alpha
     &                 *(beta**2.0D+0+gamma**2.D0+0+eta**2.0D+0)
     &                 *probe_time)
                     stop
                  endif
c     Debug
               enddo            ! o
            enddo               ! n
         enddo                  ! m
c
c     The requested temperature is T=Ts+T2
         T=Ts+T2
c     
         open(11,file=trim(results_file),access='append')
         write(11,*) (probe_x(i),i=1,dim),probe_time,Ts,T2,T
         close(11)
      enddo                     ! iprobe
      write(*,*) 'File was generated:'
      write(*,*) trim(results_file)

      return
      end
