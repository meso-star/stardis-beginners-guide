      subroutine init(temp_file,green_file,results_file)
      implicit none
      include 'max.inc'
c
c     Purpose: initialization tasks
c     
c     Input:
c       + temp_file: file used to record analytic temperatures
c       + green_file: file used to record analytic green functions
c       + results_file: file used to record MC temperatures
c
c     I/O
      character*(Nchar_mx) temp_file
      character*(Nchar_mx) green_file
      character*(Nchar_mx) results_file
c     temp
      character*(Nchar_mx) command
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine init'

      command='rm -rf '//trim(temp_file)
      call exec(command)
      command='rm -rf '//trim(green_file)
      call exec(command)
      command='rm -rf '//trim(results_file)
      call exec(command)
      
      return
      end
      
