      subroutine test_inf(value,is_inf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to test a given value against INF
c     
c     Input:
c       + value: double-precision data
c     
c     Output:
c       + is_inf: T if "value" in INF
c     
c     I/O
      double precision value
      logical is_inf
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine test_inf'
c
      if (value.eq.value-1.0D+0) then
         is_inf=.true.
      else
         is_inf=.false.
      endif
c
      return
      end
