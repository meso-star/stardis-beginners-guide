      subroutine get_temperature(debug,dim,type,
     &     boundary_index,probe_x,probe_time,T)
      implicit none
      include 'mpif.h'
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to make a single MC realization of the temperature at a
c     given position (fluid, boundary, solid) and at a given time
c     
c     Input:
c       + debug: true if debug mode
c       + dim: dimension of space
c       + type: temperature type
c         type=1: temperature at a boundary (defined by boundary_type)
c         type=2: temperature at a given position (defined by probe_x) in the solid
c       + boundary_index: index of the boundary if type=1
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c       + probe_x: value of (x,y,z) [m] if type=2
c       + probe_time: value of time [s]
c     
c     Output:
c       + T: realization of the temperature [K]
c     
c     I/O
      logical debug
      integer dim
      integer type
      integer boundary_index
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision T
c     temp
      double precision time,x(1:Nvec_mx)
      integer bindex
      double precision delta_boundary
      double precision delta_solid
      logical over
      integer i
      double precision r
      double precision p1,p2
      logical is_in_box,is_on_boundary
      double precision hconv,hrad,epsilon
      logical intersection1,intersection2
      integer boundary_index1,boundary_index2
      double precision d2b1,d2b2
      double precision u(1:Nvec_mx)
      double precision minus_u(1:Nvec_mx)
      double precision v(1:Nvec_mx)
      double precision x_init(1:Nvec_mx)
      double precision x1(1:Nvec_mx)
      double precision x2(1:Nvec_mx)
      double precision x_int1(1:Nvec_mx)
      double precision x_int2(1:Nvec_mx)
      double precision d
      double precision alpha
      double precision delta_x,dmin
      double precision delta_t
      logical keep_running
      logical int_found
      double precision dist2bound
      double precision x_int(1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine get_temperature'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
      
      time=probe_time
      call copy_vector(dim,probe_x,x)
      over=.false.

      do while (.not.over)
c     ------------------------------------------------------------------------------
         if (type.eq.1) then    ! temperature at a boundary
            call surface_temperature(dim,x,
     &           boundary_index,T)
            over=.true.
c     ------------------------------------------------------------------------------
         else if (type.eq.2) then ! temperature in the solid
            delta_solid=emin/sfactor
            keep_running=.true.
c     
            do while (keep_running)
               delta_x=delta_solid
c     - sample propagation direction
               call sample_uniform_sphere(u)
               call scalar_vector(dim,-1.0D+0,u,minus_u)
c     - look for intersections from x_init along +u and -u
               call copy_vector(dim,x,x_init)
               call scalar_vector(dim,delta_x,u,v)
               call add_vectors(dim,x_init,v,x1) ! x1=x_init+u*delta_x
               call scalar_vector(dim,delta_x,minus_u,v)
               call add_vectors(dim,x_init,v,x2) ! x2=x_init-u*delta_x
c     -------------------------------------------------------------------------
c     Intersections
               call intersect(debug,dim,x_init,u,delta_x,x1,
     &              intersection1,boundary_index1,d2b1,x_int1)
               call intersect(debug,dim,x_init,minus_u,delta_x,x2,
     &              intersection2,boundary_index2,d2b2,x_int2)
               if (intersection1) then
c     check
                  call x_is_in_box(dim,x_int1,is_in_box)
                  if (.not.is_in_box) then
                     call error(label)
                     write(*,*) 'x_int1=',(x_int1(i),i=1,dim)
                     write(*,*) 'is not in the box'
                     write(*,*) 'intersection1=',intersection1
                     if (intersection1) then
                        write(*,*) 'boundary_index1=',boundary_index1
                     endif
                     write(*,*) 'x_init=',(x_init(i),i=1,dim)
                     write(*,*) 'x1=',(x1(i),i=1,dim)
                     stop
                  endif
c     check
                  if (d2b1.lt.delta_x) then
                     delta_x=d2b1
                  endif
               endif            ! intersection1
               if (intersection2) then
c     check
                  call x_is_in_box(dim,x_int2,is_in_box)
                  if (.not.is_in_box) then
                     call error(label)
                     write(*,*) 'x_int2=',(x_int2(i),i=1,dim)
                     write(*,*) 'is not in the box'
                     write(*,*) 'intersection2=',intersection2
                     if (intersection2) then
                        write(*,*) 'boundary_index2=',boundary_index2
                     endif
                     write(*,*) 'x_init=',(x_init(i),i=1,dim)
                     write(*,*) 'x2=',(x2(i),i=1,dim)
                     stop
                  endif
c     check
                  if (d2b2.lt.delta_x) then
                     delta_x=d2b2
                  endif
               endif            ! intersection2
c     Sample +u or -u
               call random_gen(r)
               if (r.le.0.50D+0) then ! choose +u
                  call scalar_vector(dim,delta_x,u,v)
                  call add_vectors(dim,x_init,v,x) ! x=x_init+u*delta_x
               else             ! choose -u
                  call scalar_vector(dim,delta_x,minus_u,v)
                  call add_vectors(dim,x_init,v,x) ! x=x_init-u*delta_x
               endif
               call x_is_on_boundary(dim,x,is_on_boundary,bindex)
               if (is_on_boundary) then
                  type=1
                  boundary_index=bindex
                  keep_running=.false.
                  goto 111
               endif
c     
c     check
               call x_is_in_box(dim,x,is_in_box)
               if (.not.is_in_box) then
                  call error(label)
                  write(*,*) 'x=',(x(i),i=1,dim)
                  write(*,*) 'is not in the box'
                  stop
               endif
c     check
c
 111           continue
c
c     Sample a time interval according to the relevant time constant "alpha"
               alpha=6.0D+0*lambda_s/(rho_s*Cp_s*delta_x**2.0D+0)
               call sample_exp(alpha,delta_t)
c     Go back in time
               time=time-delta_t
               if (debug) then
                  write(*,*) 'x=',x,' delta_x=',delta_x,' time=',time
               endif
c     
               if (time.le.0.0D+0) then
c     If time<0, then return the initial temperature of the solid
                  call initial_solid_temperature(dim,x,T)
                  keep_running=.false.
                  over=.true.
c                  write(*,*) 'Exit because time=',time
               endif            ! if time>0 or <0
c     
            enddo               ! keep_running
c     ------------------------------------------------------------------------------
         else
            call error(label)
            write(*,*) 'type=',type
            stop
         endif
      enddo                     ! over
c      
      return
      end
      
