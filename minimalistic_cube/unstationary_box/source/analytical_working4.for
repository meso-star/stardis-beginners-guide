      subroutine analytical(dim,Nprobe,probe_positions,
     &     Nterms_fs,Nterms_pq,results_file)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
c     
c     Purpose: perform analytical computation
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Nterms_fs: number of terms in the Fourier series expansion
c       + Nterms_pq: number of terms in the sums over p and q
c       + results_file: name of the results file to generate
c     
c     Output:
c       + the "results_file" file
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      integer Nterms_fs,Nterms_pq
      character*(Nchar_mx) results_file
c     temp
      integer iprobe,i,j,m,n,o,p,q
      double precision a,b,c
      double precision Tb(1:2*Nvec_mx)
      double precision itg(1:10)
      double precision Lx2,Ly2,Lz2,Lx,Ly,Lz
      double precision alpha,beta,gamma,eta,zeta
      double precision beta2,gamma2,eta2
      double precision Cmno
      double precision sum
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision Tx,Ty,Tz,Tpq
      double precision Ts,T2,T
      double precision term1,term2,term3
      double precision z1,z2,z3
      logical is_nan,is_inf
      integer p1,q1
      logical m_is_even,n_is_even,o_is_even
      double precision G(0:6),Gs(0:6),Gi(0:6),Gtmp(1:6)
c     functions
      logical is_even
c     label
      character*(Nchar_mx) label
      label='subroutine analytical'

      write(*,*) 'Performing analytical computation...'
c     ==================================================================================
c     STEADY-STATE solution
c     ==================================================================================
      alpha=lambda_s/(rho_s*Cp_s)
      a=box(1,2)-box(1,1)
      b=box(2,2)-box(2,1)
      c=box(3,2)-box(3,1)
      Tb(1)=Tboundary(1,1)
      Tb(2)=Tboundary(1,2)
      Tb(3)=Tboundary(2,1)
      Tb(4)=Tboundary(2,2)
      Tb(5)=Tboundary(3,1)
      Tb(6)=Tboundary(3,2)
      do i=0,6
         Gs(i)=0.0D+0
      enddo                     ! i
c      
      do iprobe=1,Nprobe
         do j=1,dim
            probe_x(j)=probe_positions(iprobe,j)
         enddo                  ! j
         probe_time=probe_positions(iprobe,dim+1)
c     Tx: temperature due to (T1,T2)
         sum=0.0D+0
         do i=1,6
            Gtmp(i)=0.0D+0
         enddo                  ! i
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Lx2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Lx=dsqrt(Lx2)
               Gtmp(1)=Gtmp(1)+dsinh(Lx*(a-probe_x(1)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lx*a))
               Gtmp(2)=Gtmp(2)+dsinh(Lx*probe_x(1))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lx*a))
               sum=sum+((Tb(1)*dsinh(Lx*(a-probe_x(1)))
     &              +Tb(2)*dsinh(Lx*probe_x(1)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(Lx*a))
c     Debug
               if (dsinh(Lx*(a-probe_x(1))).gt.1.0D+300) then
                  write(*,*) p,q,Lx,dsinh(Lx*(a-probe_x(1)))
                  stop
               endif
c     Debug
            enddo               ! q
         enddo                  ! p
         Tx=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Tx,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Tx=',Tx
            write(*,*) 'Lx2=',Lx2
            write(*,*) dsinh(Lx*(a-probe_x(1)))
            write(*,*) dsinh(Lx*probe_x(1))
            stop
         endif
c     Debug
c     Ty: temperature due to (T3,T4)
         sum=0.0D+0
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Ly2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Ly=dsqrt(Ly2)
               Gtmp(3)=Gtmp(3)+dsinh(Ly*(b-probe_x(2)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Ly*b))
               Gtmp(4)=Gtmp(4)+dsinh(Ly*probe_x(2))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Ly*b))
               sum=sum+((Tb(3)*dsinh(Ly*(b-probe_x(2)))
     &              +Tb(4)*dsinh(Ly*probe_x(2)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(Ly*b))
            enddo               ! q
         enddo                  ! p
         Ty=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Ty,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Ty=',Ty
            stop
         endif
c     Debug
c     Tz: temperature due to (T5,T6)
         sum=0.0D+0
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Lz2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
               Lz=dsqrt(Lz2)
               Gtmp(5)=Gtmp(5)+dsinh(Lz*(c-probe_x(3)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lz*c))
               Gtmp(6)=Gtmp(6)+dsinh(Lz*probe_x(3))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lz*c))
               sum=sum+((Tb(5)*dsinh(Lz*(c-probe_x(3)))
     &              +Tb(6)*dsinh(Lz*probe_x(3)))
     &              *dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a))
     &              /((2.0D+0*p+1.0D+0)
     &              *(2.0D+0*q+1.0D+0)
     &              *dsinh(Lz*c))
            enddo               ! q
         enddo                  ! p
         Tz=16.0D+0*sum/(pi**2.0D+0)
c     Debug
         call test_nan(Tz,is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'Tz',Tz
            stop
         endif
c     Debug
         do i=1,6
            Gs(i)=Gs(i)+Gtmp(i)*16.0D+0/(pi**2.0D+0)
         enddo                  ! i
c     Ts: sum of Tx, Ty and Tz ; steady-state solution for (probe_x, probe_t)
         Ts=Tx+Ty+Tz
c     ==================================================================================
c     TRANSIENT solution
c     ==================================================================================
         T2=0.0D+0
         do i=0,6
            Gi(i)=0.0D+0
         enddo                  ! i
         do m=1,Nterms_fs
            beta=m*pi/a
            beta2=beta**2.0D+0
            m_is_even=is_even(m)
            do n=1,Nterms_fs
               gamma=n*pi/b
               gamma2=gamma**2.0D+0
               n_is_even=is_even(n)
               do o=1,Nterms_fs
                  eta=o*pi/c
                  eta2=eta**2.0D+0
                  o_is_even=is_even(o)
c
                  zeta=alpha*(beta2+gamma2+eta2)
c     
                  sum=0.0D+0
                  do i=1,6
                     Gtmp(i)=0.0D+0
                  enddo         ! i
                  do p=0,Nterms_pq
                     do q=0,Nterms_pq
                        Lx2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Ly2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Lz2=pi**2.0D+0*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
c     itg(1)
                        if (2*q-o+1.eq.0) then
                           itg(1)=-c/2.0D+0
                        else
                           itg(1)=0.0D+0
                        endif
c     itg(2)
                        itg(2)=eta/(eta2+Lz2)
c     itg(4)
                        if (2*p-n+1.eq.0) then
                           itg(4)=-b/2.0D+0
                        else
                           itg(4)=0.0D+0
                        endif
c     itg(5)
                        itg(5)=gamma/(gamma2+Ly2)
c     itg(7)
                        itg(7)=beta/(beta2+Lx2)
c     itg(9)
                        if (2*p-m+1.eq.0) then
                           itg(9)=-a/2.0D+0
                        else
                           itg(9)=0.0D+0
                        endif
c     itg(10)
                        if (2*q-m+1.eq.0) then
                           itg(10)=-a/2.0D+0
                        else
                           itg(10)=0.0D+0
                        endif
c     
                        if ((2*q-o+1.ne.0).or.(2*p-n+1.ne.0)) then
                           term1=0.0D+0
                        else
                           z1=itg(1)*itg(4)*itg(7)
                           Gtmp(1)=Gtmp(1)+z1
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           Gtmp(2)=Gtmp(2)+z1*(-1.0D+0)**dble(m+1)
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           term1=z1*(Tb(1)+Tb(2)*(-1.0D+0)**dble(m+1))
                        endif
                        if ((2*q-o+1.ne.0).or.(2*p-m+1.ne.0)) then
                           term2=0.0D+0
                        else
                           z2=itg(1)*itg(9)*itg(5)
                           Gtmp(3)=Gtmp(3)+z2
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           Gtmp(4)=Gtmp(4)+z2*(-1.0D+0)**dble(n+1)
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           term2=z2*(Tb(3)+Tb(4)*(-1.0D+0)**dble(n+1))
                        endif
                        if ((2*p-n+1.ne.0).or.(2*q-m+1.ne.0)) then
                           term3=0.0D+0
                        else
                           z3=itg(4)*itg(10)*itg(2)
                           Gtmp(5)=Gtmp(5)+z3
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           Gtmp(6)=Gtmp(6)+z3*(-1.0D+0)**dble(o+1)
     &                          /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                           term3=z3*(Tb(5)+Tb(6)*(-1.0D+0)**dble(o+1))
                        endif
                        Tpq=(term1+term2+term3)
     &                       /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0))
                        sum=sum+Tpq
                     enddo      ! q
                  enddo         ! p
                  if ((.not.m_is_even).and.
     &                 (.not.n_is_even).and.
     &                 (.not.o_is_even)) then
                     Gi(0)=Gi(0)+8.0D+0/(beta*gamma*eta)
     &                    *dsin(beta*probe_x(1))
     &                    *dsin(gamma*probe_x(2))
     &                    *dsin(eta*probe_x(3))
     &                    *dexp(-zeta*probe_time)
                  endif
                  do i=1,6
                     Gi(i)=Gi(i)+Gtmp(i)
     &                    *dsin(beta*probe_x(1))
     &                    *dsin(gamma*probe_x(2))
     &                    *dsin(eta*probe_x(3))
     &                    *dexp(-zeta*probe_time)
                  enddo         ! i
                  Cmno=8.0D+0/(a*b*c)*(
     &                 Tinit*(1.0D+0+(-1.0D+0)**(dble(m)+1.0D+0))
     &                 *(1.0D+0+(-1.0D+0)**(dble(n)+1.0D+0))
     &                 *(1.0D+0+(-1.0D+0)**(dble(o)+1.0D+0))
     &                 /(beta*gamma*eta)
     &                 -sum*16.0D+0/(pi**2.0D+0))
                  T2=T2
     &                 +Cmno
     &                 *dsin(beta*probe_x(1))
     &                 *dsin(gamma*probe_x(2))
     &                 *dsin(eta*probe_x(3))
     &                 *dexp(-zeta*probe_time)
c     Debug
                  call test_nan(T2,is_nan)
                  if (is_nan) then
                     call error(label)
                     write(*,*) 'T2=',T2
                     write(*,*) 'm=',m,' n=',n,' o=',o
                     write(*,*) 'Cmno=',Cmno
                     write(*,*) dsin(beta*probe_x(1))
                     write(*,*) dsin(gamma*probe_x(2))
                     write(*,*) dsin(eta*probe_x(3))
                     write(*,*) dexp(-alpha
     &                 *(beta**2.0D+0+gamma**2.D0+0+eta**2.0D+0)
     &                 *probe_time)
                     stop
                  endif
c     Debug
               enddo            ! o
            enddo               ! n
         enddo                  ! m
c     
         Gi(0)=Gi(0)*8.0D+0/(a*b*c)
         do i=1,6
            Gi(i)=-Gi(i)*128.0D+0/(a*b*c*(pi**2.0D+0))
         enddo                  ! i
c     
c     The requested temperature is T=Ts+T2
         do i=0,6
            G(i)=Gs(i)+Gi(i)
         enddo                  ! i
         T=Ts+T2
c     
         open(11,file=trim(results_file),access='append')
         write(11,*) (probe_x(i),i=1,dim),probe_time,Ts,T2,T
         close(11)
c     Debug
         T=G(0)*Tinit
         do i=1,6
            T=T+G(i)*Tb(i)
         enddo                  ! i
         write(*,*) 'T=',T
c     Debug
      enddo                     ! iprobe
      write(*,*) 'File was generated:'
      write(*,*) trim(results_file)

      return
      end
