      subroutine read_options(dim,options_file,eval_stf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the "options_file" input file
c     
c     Input
c       + dim: dimension of space
c       + options_file: file to read
c
c     Output
c       + eval_stf: true if the Solid Temperature Field must be evaluated
c     
c     I/O
      integer dim
      character*(Nchar_mx) options_file
      logical eval_stf
c     temp
      integer ios
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_options'

      open(11,file=options_file(1:strlen(options_file))
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) options_file(1:strlen(options_file))
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) eval_stf
      endif
      close(11)

c     Check for inconsistencies

      return
      end
      
