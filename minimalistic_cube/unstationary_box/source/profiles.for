      subroutine surface_temperature(dim,x,bindex,T)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the temperature at a given position over the surface of the box
c     
c     Input:
c       + dim: dimension of space
c       + x: position where the initial temperature must be evaluated (over the surface)
c       + bindex: boundary index
c       + time_index: time interval index
c     
c     Output:
c       + T: temperature @ x
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      integer bindex
      double precision T
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine surface_temperature'

      if (bindex.eq.1) then
         T=Tboundary(1,1)
      else if (bindex.eq.2) then
         T=Tboundary(1,2)
      else if (bindex.eq.3) then
         T=Tboundary(2,1)
      else if (bindex.eq.4) then
         T=Tboundary(2,2)
      else if (bindex.eq.5) then
         T=Tboundary(3,1)
      else if (bindex.eq.6) then
         T=Tboundary(3,2)
      else
         call error(label)
         write(*,*) 'bindex=',bindex
         stop
      endif
      
      return
      end


      
      subroutine initial_solid_temperature(dim,x,T)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the initial temperature at a given position in the solid
c     
c     Input:
c       + dim: dimension of space
c       + x: position where the initial temperature must be evaluated (over the surface)
c     
c     Output:
c       + T: temperature @ x
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      double precision T
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine initial_solid_temperature'

      T=Tinit                   ! K
      
      return
      end
