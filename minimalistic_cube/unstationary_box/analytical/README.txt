Calcul de températures sonde T(x,t) dans un parallélépipède rectangle:

- conditions aux limites: Dirichlet (température imposée, uniforme par face)

- condition initiale: profil uniforme

Fichiers d'entrée:

1- data.in: dimensions du parallélépipède, propriétés thermophysiques, paramètres numériques

2- boundary_conditions.in: conditions aux limites (températures des faces)
T(x=xmin) / T(x=xmax)
T(y=ymin) / T(y=ymax)
T(z=zmin) /  T(z=zmax)

3- initial_conditions.in: température initiale (uniforme dans le parallélépipède)

4- probe_positions.in: (x,t) positions sonde; par ligne:
x / y / z / t


Fichiers de sortie:

1- temperature.txt: valeurs des T(x,t); par ligne:
x / y / z / t / T(x,t)

2- green.txt: fonction de Green; par ligne:
x / y / z / t / G(t_init) / G(x=xmin) / G(x=xmax) / G(y=ymin) / G(y=ymax) / G(z=zmin) / G(z=zmax)
