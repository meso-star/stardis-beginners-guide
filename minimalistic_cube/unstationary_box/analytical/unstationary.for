      program unstationary
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c
c     Computes the temperature for a number of probe positions T(x,t)
c     for the following configuration:
c     
c     rectangular box, of known thermophysical properties, with known
c     initial temperature, and known Dirichlet conditions (temperature
c     of boundaries are set).
c     
c     Variables
      integer dim
      character*(Nchar_mx) data_file
      integer Nterms_fs,Nterms_pq
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      character*(Nchar_mx) temp_file,green_file
      double precision G(1:Np_mx,0:6)
      double precision T(1:Np_mx)
c     label
      character*(Nchar_mx) label
      label='program unstationary'

      dim=3                     ! dimension of space
      data_file='./data.in'
      call read_data(data_file,dim,
     &     box,rho_s,Cp_s,lambda_s,
     &     Tinit,Tboundary,
     &     Nterms_fs,Nterms_pq,
     &     Nprobe,probe_positions)

      call init()

      temp_file='./temperature.txt'
      green_file='./green.txt'
      call analytical(dim,Nprobe,probe_positions,
     &     Nterms_fs,Nterms_pq,temp_file,green_file,
     &     G,T)

      end
