      subroutine x_is_in_box(dim,x,is_in_box)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to check whether of not a position is in the box
c     
c     Input:
c       + dim: dimension of space
c       + x: position to check
c     
c     Output:
c       + is_in_box: true if "x" is in the box; false otherwise
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      logical is_in_box
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine x_is_in_box'

      is_in_box=.true.
      do i=1,dim
c     intersection with plane x=box(i,1)
         if ((x(i).lt.box(i,1)).or.(x(i).gt.box(i,2))) then
            is_in_box=.false.
            goto 666
         endif
      enddo                     ! i
 666  continue
      
      return
      end
