      logical function is_even(num)
      implicit none
      include 'max.inc'
c
c     Purpose: to determine whether of not a given integer value is even
c
c     Input:
c       + num: integer value to test
c
c     Output:
c       + is_even: true if "num" is even; false otherwise
c
c     I/O
      integer num

      if (dble(num)/2.0D+0.eq.dble(num/2)) then
         is_even=.true.
      else
         is_even=.false.
      endif

      return
      end
