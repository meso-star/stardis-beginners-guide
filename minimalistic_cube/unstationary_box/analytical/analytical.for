      subroutine analytical(dim,Nprobe,probe_positions,
     &     Nterms_fs,Nterms_pq,temp_file,green_file,
     &     G,T)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
c     
c     Purpose: perform analytical computation of the green function, and associated
c     temperature at a given position and date,
c     in a parallelipedic solid, with homogeneous initial condition
c     and heterogeneous boundary condition per face.
c    
c     Initial and boundary conditions have to be provided in the "Tinit" and "Tboundary"
c     arrays (defined in the "com_data.inc" include file) as follows:
c       + Tinit: initial temperature (homogeneous within the solid)
c       + Tboundary: boundary temperature, homogeneous per face, following this convention:
c         Tboundary(1,1) refers to face x=x_min 
c         Tboundary(1,2) refers to face x=x_max
c         Tboundary(2,1) refers to face y=y_min 
c         Tboundary(2,2) refers to face y=y_max
c         Tboundary(3,1) refers to face z=z_min 
c         Tboundary(3,2) refers to face z=z_max
c     
c     Physical constants of the material have to be provided in "lambda_s", "rho_s" and "Cp_s"
c     variables (also defined in the "com_data.inc" include file):
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c     
c     In practice, all relevant parameters can be set at user-level using the three
c     following input data files:
c       ./data.in: configuration and numeric parameters
c       ./data/probe_positions.in: probe positions for evaluating the temperature (x/y/z/t for each line)
c       ./data/initial_conditions.in: value of the initial temperature of the solid
c       ./data/boundary_conditions.in: value of boundary temperatures, 2 values over 3 lines
c     
c     I/O variables are described below:
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions (4D)
c       + Nterms_fs: number of terms in the Fourier series expansion
c       + Nterms_pq: number of terms in the sums over p and q
c       + temp_file: name of the file for recording temperatures
c       + green_file: name of the file for recording green functions
c     
c     Output:
c       + G: array of green function
c         - G(iprobe,0): propagation of initial condition Tinit @ (x(iprobe),t(iprobe))
c         - G(iprobe,i): propagation of boundary condition Tb(i) @ (x(iprobe),t(iprobe))
c       + T(iprobe): temperature T(x(iprobe),t(iprobe))
c       + temp_file
c       + green_file
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
      integer Nterms_fs,Nterms_pq
      character*(Nchar_mx) temp_file,green_file
      double precision G(1:Np_mx,0:6)
      double precision T(1:Np_mx)
c     temp
      integer iprobe,i,j,m,n,o,p,q
      double precision a,b,c
      double precision Tb(1:2*Nvec_mx)
      double precision itg(1:10)
      double precision Lx2,Ly2,Lz2,Lx,Ly,Lz
      double precision alpha,beta,gamma,eta,zeta
      double precision beta2,gamma2,eta2
      double precision probe_x(1:Nvec_mx)
      double precision probe_time
      double precision z1,z2,z3
      logical is_nan,is_inf
      logical m_is_even,n_is_even,o_is_even
      double precision Gs(0:6),Gi(0:6),Gtmp(1:6)
      double precision Fxyzt
      double precision pq,pi2,coeff
      double precision tmp1,tmp2,tmp3
      double precision contrib(1:6)
      integer Nt_pq
c     functions
      logical is_even
c     parameters
      double precision epsilonG
      parameter(epsilonG=1.0D-50)
c     label
      character*(Nchar_mx) label
      label='subroutine analytical'

      write(*,*) 'Performing analytical computation...'
c     ==================================================================================
c     STEADY-STATE solution
c     ==================================================================================
      alpha=lambda_s/(rho_s*Cp_s)
      a=box(1,2)-box(1,1)
      b=box(2,2)-box(2,1)
      c=box(3,2)-box(3,1)
      Tb(1)=Tboundary(1,1)
      Tb(2)=Tboundary(1,2)
      Tb(3)=Tboundary(2,1)
      Tb(4)=Tboundary(2,2)
      Tb(5)=Tboundary(3,1)
      Tb(6)=Tboundary(3,2)
      pi2=pi**2.0D+0
      coeff=16.0D+0/pi2
c      
      do iprobe=1,Nprobe
         do j=1,dim
            probe_x(j)=probe_positions(iprobe,j)
         enddo                  ! j
         probe_time=probe_positions(iprobe,dim+1)
c     Special case of probe_time=0
         if (probe_time.eq.0.0D+0) then
            G(iprobe,0)=1.0D+0
            do i=1,6
               G(iprobe,i)=0.0D+0
            enddo               ! i
            goto 123
         endif
c         
         do i=0,6
            Gs(i)=0.0D+0
         enddo                  ! i
         do i=1,6
            Gtmp(i)=0.0D+0
         enddo                  ! i
         do p=0,Nterms_pq
            do q=0,Nterms_pq
               Lx2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Ly2=pi2*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
               Lz2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &              +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
               Lx=dsqrt(Lx2)
               Ly=dsqrt(Ly2)
               Lz=dsqrt(Lz2)
               tmp1=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lx*a))
               tmp2=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(1)/a)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(3)/c)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Ly*b))
               tmp3=dsin((2.0D+0*p+1.0D+0)*pi*probe_x(2)/b)
     &              *dsin((2.0D+0*q+1.0D+0)*pi*probe_x(1)/a)
     &              /((2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
     &              *dsinh(Lz*c))
               if (tmp1.eq.0.0D+0) then
                  contrib(1)=0.0D+0
                  contrib(2)=0.0D+0
               else
                  contrib(1)=coeff*dsinh(Lx*(a-probe_x(1)))*tmp1
                  contrib(2)=coeff*dsinh(Lx*probe_x(1))*tmp1
               endif
               if (tmp2.eq.0.0D+0) then
                  contrib(3)=0.0D+0
                  contrib(4)=0.0D+0
               else
                  contrib(3)=coeff*dsinh(Ly*(b-probe_x(2)))*tmp2
                  contrib(4)=coeff*dsinh(Ly*probe_x(2))*tmp2
               endif
               if (tmp3.eq.0.0D+0) then
                  contrib(5)=0.0D+0
                  contrib(6)=0.0D+0
               else
                  contrib(5)=coeff*dsinh(Lz*(c-probe_x(3)))*tmp3
                  contrib(6)=coeff*dsinh(Lz*probe_x(3))*tmp3
               endif
               do i=1,6
                  Gtmp(i)=Gtmp(i)+contrib(i)
c     Debug
c     This is now probably useless, since the sinh() function takes
c     infinite values only when the corresponding 'tmp' variable is null.
c                  call test_nan(Gtmp(i),is_nan
                  if (isnan(Gtmp(i))) then
                     call error(label)
                     write(*,*) 'Gtmp(',i,')=',Gtmp(i)
                     stop
                  endif
c     Debug
               enddo            ! i
            enddo               ! q
         enddo                  ! p
c     Gs(i): green of Tb(i), @ (x)
         do i=1,6
            Gs(i)=Gs(i)+Gtmp(i)
         enddo                  ! i
c     ==================================================================================
c     TRANSIENT solution
c     ==================================================================================
         do i=0,6
            Gi(i)=0.0D+0
         enddo                  ! i
         Nt_pq=(Nterms_fs-1)/2
         do m=1,Nterms_fs
            beta=m*pi/a
            beta2=beta**2.0D+0
            m_is_even=is_even(m)
            do n=1,Nterms_fs
               gamma=n*pi/b
               gamma2=gamma**2.0D+0
               n_is_even=is_even(n)
               do o=1,Nterms_fs
                  eta=o*pi/c
                  eta2=eta**2.0D+0
                  o_is_even=is_even(o)
c
                  zeta=alpha*(beta2+gamma2+eta2)
                  do i=1,6
                     Gtmp(i)=0.0D+0
                  enddo         ! i
                  do p=0,Nt_pq
                     do q=0,Nt_pq
                        Lx2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Ly2=pi2*(((2.0D+0*p+1.0D+0)/a)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/c)**2.0D+0)
                        Lz2=pi2*(((2.0D+0*p+1.0D+0)/b)**2.0D+0
     &                       +((2.0D+0*q+1.0D+0)/a)**2.0D+0)
 2                     pq=(2.0D+0*p+1.0D+0)*(2.0D+0*q+1.0D+0)
c     itg(1)
                        if (2*q-o+1.eq.0) then
                           itg(1)=-c/2.0D+0
                        else
                           itg(1)=0.0D+0
                        endif
c     itg(2)
                        itg(2)=eta/(eta2+Lz2)
c     itg(4)
                        if (2*p-n+1.eq.0) then
                           itg(4)=-b/2.0D+0
                        else
                           itg(4)=0.0D+0
                        endif
c     itg(5)
                        itg(5)=gamma/(gamma2+Ly2)
c     itg(7)
                        itg(7)=beta/(beta2+Lx2)
c     itg(9)
                        if (2*p-m+1.eq.0) then
                           itg(9)=-a/2.0D+0
                        else
                           itg(9)=0.0D+0
                        endif
c     itg(10)
                        if (2*q-m+1.eq.0) then
                           itg(10)=-a/2.0D+0
                        else
                           itg(10)=0.0D+0
                        endif
c     
                        if ((2*q-o+1.eq.0).and.(2*p-n+1.eq.0)) then
                           z1=itg(1)*itg(4)*itg(7)
                           Gtmp(1)=Gtmp(1)+z1/pq
                           Gtmp(2)=Gtmp(2)+(z1*(-1.0D+0)**dble(m+1))/pq
                        endif
                        if ((2*q-o+1.eq.0).and.(2*p-m+1.eq.0)) then
                           z2=itg(1)*itg(9)*itg(5)
                           Gtmp(3)=Gtmp(3)+z2/pq
                           Gtmp(4)=Gtmp(4)+(z2*(-1.0D+0)**dble(n+1))/pq
                        endif
                        if ((2*p-n+1.eq.0).and.(2*q-m+1.eq.0)) then
                           z3=itg(4)*itg(10)*itg(2)
                           Gtmp(5)=Gtmp(5)+z3/pq
                           Gtmp(6)=Gtmp(6)+(z3*(-1.0D+0)**dble(o+1))/pq
                        endif
                     enddo      ! q
                  enddo         ! p
                  Fxyzt=dsin(beta*probe_x(1))
     &                 *dsin(gamma*probe_x(2))
     &                 *dsin(eta*probe_x(3))
     &                 *dexp(-zeta*probe_time)
                  if ((.not.m_is_even).and.
     &                 (.not.n_is_even).and.
     &                 (.not.o_is_even)) then
                     Gi(0)=Gi(0)+8.0D+0*Fxyzt/(beta*gamma*eta)
                  endif
                  do i=1,6
                     Gi(i)=Gi(i)+Gtmp(i)*Fxyzt
                  enddo         ! i
               enddo            ! o
            enddo               ! n
         enddo                  ! m
c     Gi(0): green of initial condition, @ (x,t)
         Gi(0)=Gi(0)*8.0D+0/(a*b*c)
c     Gi(i): green of Tb(i), @ (x,t)
         do i=1,6
            Gi(i)=-Gi(i)*128.0D+0/(a*b*c*pi2)
         enddo                  ! i
c     
c     Green of T(i): G(x,t)=Gs(x)+Gi(x,t)
         do i=0,6
            G(iprobe,i)=Gs(i)+Gi(i)
         enddo                  ! i
 123     continue
c     Temperature @ (x,t)
         T=G(iprobe,0)*Tinit
         do i=1,6
            T(iprobe)=T(iprobe)+G(iprobe,i)*Tb(i)
         enddo                  ! i
c     
         open(11,file=trim(temp_file),access='append')
         write(11,*) (probe_x(i),i=1,dim),probe_time,T(iprobe)
         close(11)
         open(12,file=trim(green_file),access='append')
         write(12,*) (probe_x(i),i=1,dim),probe_time,
     &        (G(iprobe,i),i=0,6)
         close(12)
c     
      enddo                     ! iprobe
      write(*,*) 'Files have been generated:'
      write(*,*) trim(temp_file)
      write(*,*) trim(green_file)

      return
      end
