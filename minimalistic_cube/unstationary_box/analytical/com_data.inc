      double precision box(1:Nvec_mx,1:2)
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Tinit
      double precision Tboundary(1:Nvec_mx,1:2)

      common /com_data1/ box
      common /com_data2/ rho_s
      common /com_data3/ Cp_s
      common /com_data4/ lambda_s
      common /com_data5/ Tinit
      common /com_data6/ Tboundary
