      subroutine read_data(data_file,dim,
     &     box,rho_s,Cp_s,lambda_s,
     &     Tinit,Tboundary,
     &     Nterms_fs,Nterms_pq,
     &     Nprobe,probe_positions)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c       + dim: dimension of space
c
c     Output:
c       + box: dimensions of the brick
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c       + Tinit: initial condition in the solid (uniform temperature)
c       + Tboundary: boundary temperatures (one per face)
c         - Tboundary(i,1): temperature of the left face, over dimension 'i'
c         - Tboundary(i,2): temperature of the right face, over dimension 'i'
c       + Nterms_fs: number of terms in the Fourier series expansion
c       + Nterms_pq: number of terms in the sums over p and q
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      double precision box(1:Nvec_mx,1:2)
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Tinit
      double precision Tboundary(1:Nvec_mx,1:2)
      integer Nterms_fs,Nterms_pq
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx+1)
c     temp
      character*(Nchar_mx) infile
      integer i,j,k,idx,ios,nlines
      integer ix,it,it_int,itmp1
      character*(Nchar_mx) nlines_file
      double precision x(1:Nvec_mx)
      logical is_in_box
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) box(1,1)
         read(10,*) box(1,2)
         read(10,*) 
         read(10,*) box(2,1)
         read(10,*) box(2,2)
         read(10,*) 
         read(10,*) box(3,1)
         read(10,*) box(3,2)
         read(10,*)
         read(10,*) rho_s
         read(10,*)
         read(10,*) Cp_s
         read(10,*)
         read(10,*) lambda_s
         do i=1,5
            read(10,*)
         enddo                  ! i
         read(10,*) Nterms_fs
         read(10,*)
         read(10,*) Nterms_pq
      endif
      close(10)
c     Read probe_positions
      infile='./probe_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Nprobe)
      if (Nprobe.gt.Np_mx) then
         call error(label)
         write(*,*) 'Nprobe=',Nprobe
         write(*,*) 'while Np_mx=',Np_mx
         stop
      endif
c     
      open(11,file=trim(infile),status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Nprobe
            read(11,*) (probe_positions(i,j),j=1,dim+1)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) trim(infile)
         stop
      endif
      close(11)

c     Read initial conditions
      infile='./initial_conditions.in'
c     
      open(11,file=trim(infile),status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         read(11,*) Tinit ! Uniform initial condition
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) trim(infile)
         stop
      endif
      close(11)
      
c     Read boundary conditions
      infile='./boundary_conditions.in'
c     
      open(11,file=trim(infile),status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,dim
            read(11,*) (Tboundary(i,j),j=1,2) ! Uniform over each face
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) trim(infile)
         stop
      endif
      close(11)
      
c     Inconsistencies
      do i=1,dim
         if (box(i,2).le.box(i,1)) then
            call error(label)
            write(*,*) 'box(',i,',2)=',box(i,2)
            write(*,*) 'should be greater than box(',i,',1)=',box(i,1)
            stop
         endif
      enddo                     ! i
      if (rho_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'rho_s=',rho_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Cp_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Cp_s=',Cp_s
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_s=',lambda_s
         write(*,*) 'should be positive'
         stop
      endif
      do i=1,Nprobe
         do j=1,dim
            x(j)=probe_positions(i,j)
         enddo
         call x_is_in_box(dim,x,is_in_box)
         if (.not.is_in_box) then
            call error(label)
            write(*,*) 'Probe position index:',i
            write(*,*) 'x=',(x(j),j=1,dim)
            write(*,*) 'is not in the box'
            stop
         endif
      enddo                     ! i
            
      return
      end

