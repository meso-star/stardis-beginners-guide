      double precision box(1:Nvec_mx,1:2)
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Tinit
      double precision Tboundary(1:Nvec_mx,1:2)
      double precision bfactor
      double precision sfactor
      double precision emin
      integer Nevent
      integer seeds(1:Nproc_mx)

      common /com_data1/ box
      common /com_data2/ rho_s
      common /com_data3/ Cp_s
      common /com_data4/ lambda_s
      common /com_data5/ Tinit
      common /com_data6/ Tboundary
      common /com_data7/ bfactor
      common /com_data8/ sfactor
      common /com_data9/ emin
      common /com_data10/ Nevent
      common /com_data11/ seeds
