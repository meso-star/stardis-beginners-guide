	integer Nvec_mx
	integer Nlay_mx
	integer Nlev_mx
	integer Nelemx_mx
	integer Nelemy_mx
	integer Nelemz_mx
	integer Nb_mx
	integer Nq_mx
	integer Nt_int_mx
	integer Nt_mx
	integer Np_mx
	integer Nterms_pq_mx
	integer Nx_mx
	integer Nmol_mx
	integer Niso_mx
	integer Niter_mx
	integer Nproc_mx
	integer Nchunk_mx
	integer Nacc_mx
	integer Nchar_mx

	parameter(Nvec_mx=3)
	parameter(Nelemx_mx=100)
	parameter(Nelemy_mx=100)
	parameter(Nelemz_mx=100)
	parameter(Nt_int_mx=10)
	parameter(Nt_mx=1000)
	parameter(Np_mx=1000)
	parameter(Nterms_pq_mx=100)
	parameter(Niter_mx=100)
	parameter(Nproc_mx=1000)
	parameter(Nchunk_mx=1000)
	parameter(Nacc_mx=100)
	parameter(Nchar_mx=10000)
