#medium
SOLID    material1   0.1   25    2   0.01    300    UNKNOWN    10   FRONT solid1.stl
SOLID    material2   0.2   20    3   0.005   300    UNKNOWN     0   FRONT solid2.stl
SOLID_SOLID_CONNECTION mat1mat2 1.0 solid2.stl

#boundary conditions
T_BOUNDARY_FOR_SOLID    LTEMP  300               cube.stl
