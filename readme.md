This repository contains all files related to the "Staris Beginners's Guide"

Prerequisites: **pdflatex** in order to compile LaTeX sources; **git-lfs** in order to download binary files (see instructions below); a **fortran compiler** in order to generate executables from fortran source files, and **gnuplot** in order to generate image files from various output files. **Stardis** can be installed following the instructions on Meso-Star's website: https://www.meso-star.com/projects/stardis/stardis.html

Binary files (i.e. image files) are stored on gitlab using git-lfs. In order to get these files, you will have to use the "git-lfs" command. After a successful cloning of the present repository, the following commands should download binary files in your local directory:

>git-lfs fetch

>git-lfs checkout


## License

The contents of this repository (LaTeX files, images and source code) are released under the CeCILL v2.1 license. You are welcome to redistribute it under certain conditions; refer to the license for details.
