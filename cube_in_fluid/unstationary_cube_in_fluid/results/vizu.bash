#!/bin/bash

#############################################################
# BEGINNING OF USER INPUT ZONE

# Name of the file that contains the high-resolution spectrum
SPECTRUM_FILE=Tsolid.txt
# Name of the file that is used by gnuplot
GNUPLOT_FILE=gra_k0

echo "HOSTNAME="$HOSTNAME
if [ $HOSTNAME == 'rhea' -o  $HOSTNAME == 'rhea.local' ]
then
    statcommand='stat -c %Y'
elif [ $HOSTNAME == 'titan' -o  $HOSTNAME == 'titan.local' ]
then
#    statcommand='stat -c %Y'
    statcommand='stat -f %m'
else
    statcommand='stat -f %m'
fi

# END OF USER INPUT ZONE
#############################################################

LIST=$SPECTRUM_FILE' '$GNUPLOT_FILE

if [ -f $SPECTRUM_FILE ]
then
    rm -f gra_k01_01
    cp gra_k gra_k0_01
    rpl "FILE" $SPECTRUM_FILE gra_k0_01

    rm -f $GNUPLOT_FILE
    
    cat plot_title.txt xlabel.txt ylabel.txt gra_k0_01 > $GNUPLOT_FILE

else
    echo "File is missing: "$SPECTRUM_FILE
    exit 1
fi # -f $SPECTRUM_FILE

echo "Press CTRL+C to quit this script..."

s0=0
for file in $LIST
do
    if [ -f $file ]
    then
#	s0=$(($s0+$($statcommand -c %Y $file)))
	s0=$(($s0+$($statcommand $file)))
    fi
done

i=1
while [ $i -gt 0 ]
do
    gnuplot gra_res
    if [ $i -eq 1 ]
    then
	gv -watch res.pdf &
    fi
    
    s1=$s0
    while [ $s1 -eq $s0 ]
    do
	s1=0
	for file in $LIST
	do
	    if [ -f $file ]
	    then
#		s1=$(($s1+$($statcommand -c %Y $file)))
		s1=$(($s1+$($statcommand $file)))
	    fi
	done
	sleep 1s
    done
    s0=$s1
    
    i=$[$i+1]
done
exit 0
