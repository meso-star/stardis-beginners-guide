Simple template for working on thermal computations using the MC method
(coupling of conduction, convection and radiation into a single algorithm).

Geometry: a parallelipedic box

Initial temperature spatial profile: uniform

External temperature temporal profiles: uiform
