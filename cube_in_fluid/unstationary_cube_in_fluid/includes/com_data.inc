      double precision box(1:Nvec_mx,1:2)
      double precision hc
      double precision hr
      double precision epsilon
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Tbase
      double precision Text
      double precision Trad_ext
      double precision bfactor
      double precision sfactor
      double precision emin
      integer Nevent
      integer seeds(1:Nproc_mx)

      common /com_data1/ box
      common /com_data2/ hc
      common /com_data3/ hr
      common /com_data4/ epsilon
      common /com_data5/ rho_s
      common /com_data6/ Cp_s
      common /com_data7/ lambda_s
      common /com_data8/ Tbase
      common /com_data9/ Text
      common /com_data10/ Trad_ext
      common /com_data11/ bfactor
      common /com_data12/ sfactor
      common /com_data13/ emin
      common /com_data14/ Nevent
      common /com_data15/ seeds
