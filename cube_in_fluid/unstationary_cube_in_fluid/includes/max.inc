	integer Nvec_mx
	integer Nlay_mx
	integer Nlev_mx
	integer Nelem_mx
	integer Nb_mx
	integer Nq_mx
	integer Nt_mx
	integer Np_mx
	integer Nx_mx
	integer Nmol_mx
	integer Niso_mx
	integer Niter_mx
	integer Nproc_mx
	integer Nchunk_mx
	integer Nacc_mx
	integer Nchar_mx

	parameter(Nvec_mx=3)
	parameter(Nlay_mx=100)
	parameter(Nlev_mx=Nlay_mx+1)
	parameter(Nelem_mx=1000)
	parameter(Nb_mx=800)
	parameter(Nq_mx=20)
	parameter(Nt_mx=1000)
	parameter(Np_mx=100)
	parameter(Nx_mx=10)
	parameter(Nmol_mx=50)
	parameter(Niso_mx=10)
	parameter(Niter_mx=100)
	parameter(Nproc_mx=1000)
	parameter(Nchunk_mx=1000)
	parameter(Nacc_mx=100)
	parameter(Nchar_mx=10000)
