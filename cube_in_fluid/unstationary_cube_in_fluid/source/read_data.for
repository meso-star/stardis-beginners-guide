      subroutine read_data(data_file,dim,
     &     box,hc,hr,epsilon,rho_s,Cp_s,lambda_s,
     &     Tbase,Text,Trad_ext,bfactor,sfactor,Nevent,
     &     probe_position,total_time,Ntime)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c       + dim: dimension of space
c
c     Output:
c       + box: dimensions of the brick
c       + hc: convective exchange coefficient  [W/(m^2.K)]
c       + hr: radiative exchange coefficient  [W/(m^2.K)]
c       + epsilon: emissivity of the solid
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c       + Tbase: initial temperature in the solid [K]
c       + Text: temperature of the external fluid [K]
c       + Trad_ext: external radiative exchange temperature [K]
c       + bfactor: delta_boundary=e/bfactor
c       + sfactor: delta_solid=e/sfactor
c       + Nevent: number of statistical realizations
c       + probe_position: probe position if temporal profile is required
c       + total_time: time for temperature estimation
c       + Ntime: number of time points
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      double precision box(1:Nvec_mx,1:2)
      double precision hc
      double precision hr
      double precision epsilon
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Tbase
      double precision Text
      double precision Trad_ext
      double precision bfactor
      double precision sfactor
      integer Nevent
      double precision probe_position(1:Nvec_mx)
      double precision total_time
      integer Ntime
      character*(Nchar_mx) infile
c     temp
      integer i,ios
      logical is_in_box
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=data_file(1:strlen(data_file)),
     &     status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) data_file(1:strlen(data_file))
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) box(1,1)
         read(10,*) box(1,2)
         read(10,*) 
         read(10,*) box(2,1)
         read(10,*) box(2,2)
         read(10,*) 
         read(10,*) box(3,1)
         read(10,*) box(3,2)
         read(10,*)
         read(10,*) hc
         read(10,*)
         read(10,*) hr
         read(10,*)
         read(10,*) epsilon
         read(10,*)
         read(10,*) rho_s
         read(10,*)
         read(10,*) Cp_s
         read(10,*)
         read(10,*) lambda_s
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Tbase
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Text
         read(10,*)
         read(10,*) Trad_ext
         do i=1,5
            read(10,*)
         enddo                  ! i
         read(10,*) bfactor
         read(10,*)
         read(10,*) sfactor
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nevent
         read(10,*)
         do i=1,dim
            read(10,*) probe_position(i)
         enddo                  ! i
         read(10,*)
         read(10,*) total_time
         read(10,*)
         read(10,*) Ntime
      endif
      close(10)      

c     Inconsistencies
      do i=1,dim
         if (box(i,2).le.box(i,1)) then
            call error(label)
            write(*,*) 'box(',i,',2)=',box(i,2)
            write(*,*) 'should be greater than box(',i,',1)=',box(i,1)
            stop
         endif
      enddo                     ! i
      if (hc.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hc=',hc
         write(*,*) 'should be positive'
         stop
      endif
      if (hr.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hr=',hr
         write(*,*) 'should be positive'
         stop
      endif
      if (epsilon.lt.0.0D+0) then
         call error(label)
         write(*,*) 'epsilon=',epsilon
         write(*,*) 'should be positive'
         stop
      endif
      if (rho_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'rho_s=',rho_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Cp_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Cp_s=',Cp_s
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_s=',lambda_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Tbase.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Tbase=',Tbase
         write(*,*) 'should be positive'
         stop
      endif
      if (Text.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Text=',Text
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad_ext.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Trad_ext=',Trad_ext
         write(*,*) 'should be positive'
         stop
      endif
      if (bfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'bfactor=',bfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (sfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'sfactor=',sfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
      call x_is_in_box(dim,probe_position,is_in_box)
      if (.not.is_in_box) then
         call error(label)
         write(*,*) 'probe_position=',(probe_position(i),i=1,dim)
         write(*,*) 'is not in the box'
         stop
      endif
      if (total_time.lt.0.0D+0) then
         call error(label)
         write(*,*) 'total_time=',total_time
         write(*,*) 'should be positive'
         stop
      endif
      if (Ntime.le.0) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) 'should be positive'
         stop
      endif

      return
      end

