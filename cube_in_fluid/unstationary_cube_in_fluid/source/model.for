      subroutine Tfluid(debug,dim,time,Tf)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'com_data.inc'
c      
c     Purpose: to compute the temperature of the fluid
c     
c     Input:
c       + time: current time [s]
c       + dim: dimension of space
c       + boundary_index: 1 for inner sphere; 2 for outer sphere
c     
c     Output:
c       + time (updated)
c       + Tf: temperature of the fluid @ time
c     
c     I/O
      logical debug
      integer dim
      double precision time
      double precision Tf
c     temp
      double precision S,V
      double precision alpha
      double precision delta_t
      double precision p1
      double precision r
      double precision delta_boundary
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Tfluid'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
c     In this case, the fluid is regulated at a given temperature Text
      Tf=Text
c     This temperature could be a temporal profile so I let "time" as an input
      
      return
      end
      


      subroutine Tboundary(debug,dim,time,x,boundary_index,
     &     delta_boundary,Tb)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to compute a boundary temperature
c     
c     Input:
c       + time: current time [s]
c       + dim: dimension of space
c       + x: position on the surface
c       + boundary_index: index of the boundary if type=1
c         - boundary_index=1: x=x_min
c         - boundary_index=2: x=x_max
c         - boundary_index=3: y=y_min
c         - boundary_index=4: y=y_max
c         - boundary_index=5: z=z_min
c         - boundary_index=6: z=z_max
c       + delta_boundary: delta @ boundary
c     
c     Output:
c       + Tb: boundary temperature
c
c     I/O
      logical debug
      integer dim
      double precision time
      double precision x(1:Nvec_mx)
      integer boundary_index
      double precision delta_boundary
      double precision Tb
c     temp
      integer i
      double precision r
      double precision p1,p2
      double precision delta_solid
      logical is_in_box
      double precision hconv,hrad
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Tboundary'
      
      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
      call exchange_coefficients(dim,boundary_index,hconv,hrad)
c     p1 is the probability to evaluate the temperature of the fluid
      p1=hconv/(hconv+hrad*epsilon+lambda_s/delta_boundary)
c     p2 is the probability to evaluate the radiative temperature
      p2=hrad*epsilon/(hconv+hrad*epsilon+lambda_s/delta_boundary)

      call random_gen(r)
      if (r.le.p1) then
c     Temperature of the fluid
         call Tfluid(debug,dim,time,Tb)
      else if (r.le.p1+p2) then
c     Radiative temperature
         call Trad(debug,dim,time,Tb)
      else
c     Conductive branch
         if (boundary_index.eq.1) then
            x(1)=box(1,1)+delta_boundary
         else if (boundary_index.eq.2) then
            x(1)=box(1,2)-delta_boundary
         else if (boundary_index.eq.3) then
            x(2)=box(2,1)+delta_boundary
         else if (boundary_index.eq.4) then
            x(2)=box(2,2)-delta_boundary
         else if (boundary_index.eq.5) then
            x(3)=box(3,1)+delta_boundary
         else if (boundary_index.eq.6) then
            x(3)=box(3,2)-delta_boundary
         else
            call error(label)
            write(*,*) 'boundary_index=',boundary_index
            write(*,*) 'allowed values: 1-6'
            stop
         endif
         delta_solid=emin/sfactor
c     check
         call x_is_in_box(dim,x,is_in_box)
         if (.not.is_in_box) then
            call error(label)
            write(*,*) 'x=',(x(i),i=1,dim)
            write(*,*) 'is not in the box'
            stop
         endif
c     check
         call Tsolid(debug,dim,time,x,delta_solid,Tb)
      endif

      return
      end


      
      subroutine Trad(debug,dim,time,Tr)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c      
c     Purpose: to compute the radiative temperature
c     
c     Input:
c       + time: current time [s]
c       + dim: dimension of space
c     
c     Output:
c       + time (updated)
c       + Tr: radiative temperature
c     
c     I/O
      logical debug
      integer dim
      double precision time
      double precision Tr
c     temp
      double precision p1
      double precision r
      double precision delta_boundary
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Trad'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
      endif
c     In this case, the radiative temperature is set to a constant value Trad_ext
      Tr=Trad_ext
c     This temperature could be a temporal profile, so I let the "time" input

      return
      end

      

      subroutine Tsolid(debug,dim,time,x,delta_solid,Ts)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to compute a surface within the solid
c     This routine is based on 1D diffusion (plane-parallel hypothesis)
c     
c     Input:
c       + time: current time [s]
c       + dim: dimension of space
c       + x: position in the solid
c       + delta_solid: diffusion step in the solid
c     
c     Output:
c       + time: updated
c       + Ts: temperature in the solid
c
c     I/O
      logical debug
      integer dim
      double precision time
      double precision x(1:Nvec_mx)
      double precision delta_solid
      double precision Ts
c     temp
      integer i
      logical intersection
      integer boundary_index
      double precision d2b
      double precision u(1:Nvec_mx)
      double precision v(1:Nvec_mx)
      double precision x_init(1:Nvec_mx)
      double precision x1(1:Nvec_mx)
      double precision d
      double precision alpha
      double precision delta_x,dmin
      double precision delta_t
      double precision delta_boundary
      double precision r
      logical keep_running
      logical is_in_box
      logical int_found
      integer bindex
      double precision dist2bound
      double precision x_int(1:Nvec_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine Tsolid'

      if (debug) then
         write(*,*) 'In: ',label(1:strlen(label))
         write(*,*) 'x=',x,' delta_solid=',delta_solid,' time=',time
      endif
      keep_running=.true.
      delta_x=delta_solid

      do while (keep_running)
c     Sample a time interval according to the relevant time constant "alpha"
         alpha=2.0D+0*lambda_s/(rho_s*Cp_s*delta_x**2.0D+0)
         call sample_exp(alpha,delta_t)
c     Go back in time
         time=time-delta_t
         if (debug) then
            write(*,*) 'x=',x,' delta_x=',delta_x,' time=',time
         endif
c     
         if (time.le.0.0D+0) then
c     If time<0, then return the initial temperature of the solid
            call initial_solid_temperature(dim,x,Ts)
            keep_running=.false.
            if (debug) then
               write(*,*) 'Exit because time=',time
            endif
         else
c     If time>0, get the temperature next to x:
c     - sample propagation direction
            call sample_uniform_sphere(u)
c     - propagate x along u
            call copy_vector(dim,x,x_init)
            call scalar_vector(dim,delta_x,u,v)
            call add_vectors(dim,x_init,v,x1)
c     -------------------------------------------------------------------------
c     Intersection
            call intersect(debug,dim,x_init,u,delta_x,x1,
     &           intersection,boundary_index,d2b,x)
            if (intersection) then
c     check
               call x_is_in_box(dim,x,is_in_box)
               if (.not.is_in_box) then
                  call error(label)
                  write(*,*) 'x=',(x(i),i=1,dim)
                  write(*,*) 'is not in the box'
                  write(*,*) 'intersection=',intersection
                  if (intersection) then
                     write(*,*) 'boundary_index=',boundary_index
                  endif
                  write(*,*) 'x_init=',(x_init(i),i=1,dim)
                  write(*,*) 'x1=',(x1(i),i=1,dim)
                  stop
               endif
c     check
               delta_boundary=emin/bfactor
               call Tboundary(debug,dim,time,x,boundary_index,
     &              delta_boundary,Ts)
               keep_running=.false.
               if (debug) then
                  write(*,*) 'Exit because x=',x
               endif
               goto 111
            else
               call copy_vector(dim,x1,x)
            endif
c     -------------------------------------------------------------------------
c     Modification of the spatial step according to boundary proximity
            call d2boundary(debug,dim,x_init,u,
     &           int_found,bindex,dist2bound,x_int)
            if (dist2bound.le.delta_x) then
               dmin=dist2bound
               if (dmin.le.0.0D+0) then
                  call error(label)
                  write(*,*) 'dmin=',dmin
                  stop
               else
                  delta_x=dmin
                  if (delta_x.lt.emin*1.0D-8) then
                     delta_boundary=emin/bfactor
                     call Tboundary(debug,dim,time,x_int,bindex,
     &                    delta_boundary,Ts)
                     keep_running=.false.
                     if (debug) then
                        write(*,*) 'Exit because x=',x_int
                     endif
                     goto 111
                  endif
               endif
            else
               if (delta_x.ge.delta_solid) then
                  delta_x=delta_solid
               endif
            endif
 111        continue
         endif                  ! if time>0 or <0
      enddo ! keep_running
      
      return
      end
      
