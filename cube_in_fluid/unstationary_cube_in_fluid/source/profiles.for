      subroutine initial_solid_temperature(dim,x,Tinit)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the initial temperature at a given position
c     
c     Input:
c       + dim: dimension of space
c       + x: position where the initial temperature must be evaluated
c     
c     Output:
c       + Tinit: initial temperature @ x
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      double precision Tinit
c     temp
      double precision t(1:Nvec_mx)
      double precision d,nf
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine initial_solid_temperature'

      Tinit=Tbase

      return
      end


      
      subroutine normal_function(sigma,d,f)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to evaluate the normal function
c     
c     Input:
c       + sigma: standard deviation
c       + d: distance to center
c     
c     Output:
c       + f: value of the normal function @ d
c     
c     I/O
      double precision sigma
      double precision d
      double precision f
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine normal_function'

      f=dexp(-d**2.0D+0/(2.0D+0*sigma**2.0D+0))
c     &     /dsqrt(2.0D+0*pi*sigma**2.0D+0)

      return
      end
      


      subroutine exchange_coefficients(dim,boundary_index,hconv,hrad)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to provide convective and radiative exchange coefficients at a given boundary
c     
c     Input:
c       + dim: dimension of space
c       + boundary_index: index of the boundary where exchange coefficients must be evaluated
c     
c     Output:
c       + hconv: convective exchange coefficient
c       + hrad: radiative exchange coefficient
c     
c     I/O
      integer dim
      integer boundary_index
      double precision hconv,hrad
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine exchange coefficients'

      hconv=hc
      hrad=hr

      return
      end
      
