c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine get_nlines(file,nlines_file,nl)
      implicit none
      include 'max.inc'
c
c     Purpose: to find the number of lines in a given file
c
c     Inputs:
c       + file: file whose number of lines has to be found
c       + nlines_file: temporary file to use
c     
c     Outputs:
c       + nl: number of lines within "file"
c

c     I/O
      character*(Nchar_mx) file
      character*(Nchar_mx) nlines_file
      integer nl
c     temp
      integer ios
      character*(Nchar_mx) command
      integer strlen
      character*(Nchar_mx) label
      label='subroutine get_nlines'

      open(10,file=file(1:strlen(file))
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         nl=0
         goto 123
      endif

c     Number of lines in the specified data file
      command='wc -l < '
     &     //file(1:strlen(file))
     &     //' > ./'
     &     //nlines_file(1:strlen(nlines_file))
      call exec(command)

      open(10,file=nlines_file(1:strlen(nlines_file)))
      read(10,*) nl
      close(10)

 123  continue

      return
      end
