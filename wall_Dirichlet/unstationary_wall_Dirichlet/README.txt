Calcul du profil de temp�rature spatio/temporel dans un mur (g�om�trie slab), avec:

- conditions aux limites:
+ � gauche: temp�rature impos�e (temp. initiale + delta T)
+ � droite : temp�rature impos�e

- condition initiale: profil stationnaire provenant des conditions aux limites suivantes:
+ � gauche: temp�rature impos�e
+ � droite: temp�rature impos�e
