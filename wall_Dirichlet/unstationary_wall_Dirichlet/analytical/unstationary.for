      program unstationary
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to compute probe temperatures T(x,t) for a number
c     of probe positions / probe times in the following configuration:
c     a wall of given thickness and infinite other dimensions, with
c     known thermophysial properties and volumic source term.
c     
c     Initial conditions: known temperatures for the left and right
c     boundaries.
c     
c     At t=0: a different temperature is imposed on the left boundary.
c     
c     Variables
      integer dim
      character*(Nchar_mx) data_file
      integer Nterms
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     Label
      character*(Nchar_mx) label
      label='program unstationary'

      dim=3                     ! dimension of space
      data_file='./data.in'
      call read_data(data_file,dim,
     &     box,rho_s,Cp_s,lambda_s,P,
     &     Tb1,Delta_Tb1,Tb2,
     &     Nterms,Nprobe,probe_positions,Ntime,time_positions)

      call init()

      call analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      
      end
