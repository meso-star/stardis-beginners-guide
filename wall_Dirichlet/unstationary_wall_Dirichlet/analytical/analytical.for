      subroutine analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to evaluate analytical temperature fields
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c       + Nterms: number of terms in the Fourier series expansion
c     
c     Output:
c       + Tunstationary: temperature profile at each (x/t) positions
c       + Tinit: initial temperature profile
c       + Tstationary: stationary (final) temperature profile
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Np_mx)
      integer Nterms
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     temp
      integer n
      integer itime,iprobe,iterm
      double precision time,probe_x
      double precision a(1:Nvec_mx,1:Nvec_mx)
      double precision y(1:Nvec_mx)
      double precision dbeta
      logical check,possible_root,keep_looking
      double precision max_err
      logical valid
      double precision Tb011,Tb012
      double precision Tb021,Tb022
      double precision as,bs
      double precision int_sin,int_xsin,int_x2sin
      double precision Ts1,Ts2
      double precision x(1:Nvec_mx)
      double precision sin2betae
      double precision norm,norm_theoric
      double precision coeffa,coeffb
      double precision coeff(1:Nterms_mx)
      character*(Nchar_mx) str
      integer err
c     temp
      integer i,m
      logical debug
      double precision Tb11,Tb21,Ts_analytic
      double precision e,h1,h2
      double precision beta,beta1,beta2
      double precision f,f1,f2
      double precision errx,errf
      logical solution_found
      double precision beta0
      integer Np
      integer Nroots
      double precision root(1:Nterms_mx)
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
      character*(Nchar_mx) infile
c     functions
      double precision fbeta
c     label
      character*(Nchar_mx) label
      label='subroutine analytical_results'

      e=(box(1,2)-box(1,1))
      
c     Initial temperature profile
      Tb10=Tb1
      Tb20=Tb2
      infile='./Tsolid_initial.txt'
      open(11,file=trim(infile))
      write(11,10) 'Temperatures @ intial conditions'
      write(11,*)
      write(11,10) 'Temperature on the left boundary:'
      write(11,*) Tb10
      write(11,10) 'Temperature on the right boundary:'
      write(11,*) Tb20
      write(11,*)            
      do i=1,Nprobe
         Ts_analytic=Tb10
     &        +(Tb20-Tb10)
     &        *(probe_positions(i,1)-box(1,1))/e
         write(11,*) probe_positions(i,1),Ts_analytic
         Tinit(i)=Ts_analytic
      enddo                     ! i
      close(11)
      write(*,*) 'File was generated: ',trim(infile)
c     Stationary temperature profile
      Tb11=Tb1+Delta_Tb1
      Tb21=Tb2
      infile='./Tsolid_stationary.txt'
      open(11,file=trim(infile))
      write(11,10) 'Temperatures @ stationary'
      write(11,*)
      write(11,10) 'Temperature on the left boundary:'
      write(11,*) Tb11
      write(11,10) 'Temperature on the right boundary:'
      write(11,*) Tb21
      write(11,*)
      bs=Tb11
      as=(Tb21-bs+(P*e**2.0D+0)/(2.0D+0*lambda_s))/e
      do i=1,Nprobe
         probe_x=probe_positions(i,1)-box(1,1)
         Ts_analytic=-(P*probe_x**2.0D+0)/(2.0D+0*lambda_s)
     &        +as*probe_x+bs
         write(11,*) probe_x,Ts_analytic
         Tstationary(i)=Ts_analytic
      enddo                     ! i
      close(11)
      write(*,*) 'File was generated: ',trim(infile)

c     unstationary temperature profile
c     Solving T1, the unstationary homogeneous problem
      coeffa=Delta_Tb1/e
      coeffb=-Delta_Tb1
      do itime=1,Ntime
         time=time_positions(itime)
         do iprobe=1,Nprobe
            probe_x=probe_positions(iprobe,1)-box(1,1)
            if (time.le.0.0D+0) then
               Ts_analytic=Tb10
     &              +(Tb20-Tb10)
     &              *(probe_x-box(1,1))/e
            else
c     Initialization: solution of then stationary problem
               Ts1=Tstationary(iprobe)
c     Fourier series evaluation
               Ts2=0.0D+0
               do iterm=1,Nterms
c     coeff(iterm)=-2.0D+0/(dble(iterm)*pi)*Delta_Tb1
                  int_sin=e*(1.0D+0+(-1.0D+0)**dble(iterm+1))
     &                 /(dble(iterm)*pi)
                  int_xsin=((-1.0D+0)**dble(iterm+1))*(e**2.0D+0)
     &                 /(dble(iterm)*pi)
                  int_x2sin=((-1.0D+0)**dble(iterm+1))
     &                 *(e**3.0D+0)/(dble(iterm)*pi)
     &                 -2.0D+0*(1.0D+0+(-1.0D+0)**dble(iterm+1))
     &                 *(e/(dble(iterm)*pi))**3.0D+0
                  coeff(iterm)=((Tb10-bs)*int_sin
     &                 +((Tb20-Tb10)/e-as)*int_xsin
     &                 +P/(2.0D+0*lambda_s)*int_x2sin)*2.0D+0/e
                  Ts2=Ts2
     &                 +coeff(iterm) ! Fourier coefficient
     &                 *dsin(dble(iterm)*pi*probe_x/e) ! function of space
     &                 *dexp(-lambda_s/(rho_s*Cp_s)
     &                 *((dble(iterm)*pi/e)**2.0D+0)*time) ! function of time
               enddo            ! iterm
               Ts_analytic=Ts1+Ts2
            endif ! value of time
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
            call num2str(itime,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'itime=',itime
               stop
            else
               results_file_t='./Tsolid_analytical_t'
     &              //trim(str)
     &              //'.txt'
            endif
            open(11,file=trim(results_file_t),access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
            write(*,*) 'File was generated: ',trim(results_file_t)
c     + space files
            call num2str(iprobe,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'iprobe=',iprobe
               stop
            else
               results_file_x='./Tsolid_analytical_x'
     &              //trim(str)
     &              //'.txt'
            endif
            open(11,file=trim(results_file_x),access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
            write(*,*) 'File was generated: ',trim(results_file_x)
c     --------------------------------------------------------------------------------------
         enddo                  ! iprobe
         close(11)
      enddo                     ! itime
      
      return
      end



      subroutine solve_fbeta_root(e,h1,h2,m,beta1,beta2,errx,errf,
     &     solution_found,beta0)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a root of f(beta)=0 between two known boundaries
c     
c     Input:
c       + e: wall thickness [m]
c       + h1: total exchange coefficient of the left
c       + h2: total exchange coefficient of the right
c       + m: index of the period
c       + beta1: lower beta boundary
c       + beta2: lower beta boundary
c       + errx: maximum error over x (beta)
c       + errf: maximum error over f
c    
c    Output:
c      + solution_found: true if a solution has been found
c      + beta0: f(beta0)=0
c    
c    I/O
      double precision e,h1,h2
      integer m
      double precision beta1,beta2
      double precision errx,errf
      logical solution_found
      double precision beta0
c     temp
      double precision beta_mid
      double precision f1,f2,f_mid
      integer iter
      logical keep_running
c     functions
      double precision fbeta
c     label
      character*(Nchar_mx) label
      label='subroutine solve_fbeta_root'

      f1=fbeta(e,h1,h2,beta1,m)
      f2=fbeta(e,h1,h2,beta2,m)
      keep_running=.true.
      iter=0
      do while (keep_running)
         iter=iter+1
         beta_mid=(beta1+beta2)/2.0D+0
         f_mid=fbeta(e,h1,h2,beta_mid,m)
         if (f1*f_mid.lt.0.0D+0) then
            beta2=beta_mid
            f2=f_mid
         else if (f_mid*f2.lt.0.0D+0) then
            beta1=beta_mid
            f1=f_mid
         else
            call error(label)
            write(*,*) 'beta1=',beta1,' f1=',f1
            write(*,*) 'beta2=',beta2,' f2=',f2
            write(*,*) 'beta_mid=',beta_mid,' f_mid=',f_mid
            write(*,*) 'f1*f_mid=',f1*f_mid
            write(*,*) 'f2*f_mid=',f2*f_mid
            write(*,*) 'one should be negative'
            stop
         endif
c     Debug
c         write(*,*) iter,beta1,beta2,dabs(beta2-beta1),dabs(f2-f1)
c     Debug
         if (dabs(beta2-beta1).lt.errx) then
            solution_found=.true.
            beta0=beta_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'beta2-beta1=',dabs(beta2-beta1),beta0
c     Debug
         endif
         if (dabs(f2-f1).lt.errf) then
            solution_found=.true.
            beta0=beta_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'f2-f1=',dabs(f2-f1),beta0
c     Debug
         endif
         if (iter.gt.Niter_mx) then
            solution_found=.false.
            keep_running=.false.
         endif
      enddo ! while (keep_running)

      return
      end



      double precision function fbeta(e,h1,h2,beta,m)
      implicit none
      include 'param.inc'
c      
c     Purpose: to evaluate the function of beta which roots
c     have to be found
c     
c     Input:
c       + e: wall thickness [m]
c       + h1: total exchange coefficient of the left
c       + h2: total exchange coefficient of the right
c       + beta: value of beta where the function has to be evaluated
c       + m: index of the period
c    
c     Output:
c       + fbeta: value of the function for beta
c     
c     I/O
      double precision e,h1,h2,beta
      integer m
      
      fbeta=dtan(2.0D+0*pi*(m-1)+beta*e)
     &     -beta*(h1+h2)/(beta**2.0D+0-h1*h2)
      
      return
      end
      
