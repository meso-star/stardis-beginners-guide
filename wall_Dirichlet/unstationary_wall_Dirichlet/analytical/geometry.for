      subroutine distance_cart(x1,y1,z1,x2,y2,z2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their cartesian coordinates
c
c     inputs
      double precision x1,y1,z1
      double precision x2,y2,z2
c     outputs
      double precision d
c     label
      character*(Nchar_mx) label
      label='subroutine distance_cart'

      d=dsqrt((x2-x1)**2.0D+0+(y2-y1)**2.0D+0+(z2-z1)**2.0D+0)

      return
      end
