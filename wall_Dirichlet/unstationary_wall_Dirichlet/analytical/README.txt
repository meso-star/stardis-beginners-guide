Calcul du profil de temp�rature spatio/temporel dans un mur (g�om�trie slab), avec:

- condition initiale: profil stationnaire provenant des conditions aux limites suivantes:
+ � gauche: Dirichlet (temp�rature impos�e)
+ � droite: Dirichlet (temp�rature impos�e)

- conditions aux limites pour t>0:
idem que pr�c�demment + un shift de temp�rature est impos� � gauche

Fichiers d'entr�e:

1- data.in: propri�t�s thermophysiques, terme source volumique, condition initiale
et aux limites, param�tres num�riques.

2- probe_positions.in: positions sonde; par ligne: x / y / z

3- time_positions.in: temps sonde; par ligne: t


Fichiers de sortie:

1- Tsolid_analytical_t*.txt: temp�ratures sonde, pour chaque valeur du temps sonde;
par ligne:
x / T(x,t)

1- Tsolid_analytical_x*.txt: temp�ratures sonde, pour chaque valeur de la position sonde;
par ligne:
x / t / T(x,t)

