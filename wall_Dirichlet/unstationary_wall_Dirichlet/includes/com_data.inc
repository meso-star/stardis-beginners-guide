      double precision box(1:Nvec_mx,1:2)
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision P
      double precision Tb1
      double precision Delta_Tb1
      double precision Tb2
      double precision bfactor
      double precision sfactor
      double precision emin
      double precision Tb10
      double precision Tb20
      integer Nevent
      integer seeds(1:Nproc_mx)

      common /com_data1/ box
      common /com_data2/ rho_s
      common /com_data3/ Cp_s
      common /com_data14/ lambda_s
      common /com_data5/ P
      common /com_data6/ Tb1
      common /com_data7/ Delta_Tb1
      common /com_data8/ Tb2
      common /com_data9/ bfactor
      common /com_data10/ sfactor
      common /com_data11/ emin
      common /com_data12/ Tb10
      common /com_data13/ Tb20
      common /com_data14/ Nevent
      common /com_data15/ seeds
