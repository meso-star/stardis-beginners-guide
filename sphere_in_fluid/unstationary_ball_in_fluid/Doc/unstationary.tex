\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
%\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
%\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
%\usepackage{auto-pst-pdf}
\usepackage{pstricks,pst-node,pst-text,pst-3d}
%\usepackage{mathbbm}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
%\usepackage[3D]{movie15}
%\usepackage[]{endfloat}
\usepackage{animate}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\newcommand{\ul}{\underline}
%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-.5in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{.125in}
\setlength{\textwidth}{6.25in}


\begin{document}

\title{Thermique analytique instationnaire}

\maketitle
\begin{center}
\author{V. Eymet, Méso-Star (\url{http://www.meso-star.com})} \\
\end{center}
\vspace{1cm}

\newpage
\tableofcontents

\newpage
\section{Introduction}

L'avantage de disposer de solutions analytiques est la possibilité de valider les algorithmes de Monte-Carlo dans des situations non triviales: certes les géométries traitées dans ce document sont simples, mais à priori ce n'est pas l'objet pour MC puisque la partie géométrique est traitée de façon indépendante de la géométrie. Le principal intérêt des solutions analytiques recensées ici est de disposer du profil de température temporel.

\section{Géométrie slab}


\subsection{Problème}

On souhaite trouver de façon analytique le champ de température instationnaire dans un mur homogène (géométrie slab d'épaisseur $e$, de conductivité $\lambda$, de masse volumique $\rho$ et de capacité calorifique $C_{p}$). Les conditions aux limites sont de type convection et rayonnement sur les deux faces du mur et la condition initiale est donnée par une fonction $f(x)=T(x,t=0)$.

L'application sera réalisée pour les conditions suivantes:
\begin{itemize}
\item Condition initiale: le mur est initialement dans un état stationnaire où, du côté $x=0$, les échanges thermiques s'effectuent par convection (coefficient d'échange par convection $h_{c1}$, température d'air $T_{f1}$) et par rayonnement (coefficient d'échange linéarisé par rayonnement $h_{r1}$, température d'ambiance radiative uniforme $T_{r1}$); du côté $x=e$, les échanges thermiques s'effectuent également par convection et par rayonnement, avec des constantes différentes: échange convectif de coefficient $h_{c2}$ avec un air à température $T_{f2}$, échange radiatif de coefficient linéarisé $h_{r2}$ avec une température d'ambiance radiative uniforme $T_{r2}$. Le profil de température initial $f(x)$ est donc linéaire entre deux températures $T_{0}(0)$ et $T_{0}(e)$ que l'on peut déterminer facilement de façon analytique ou numérique (voir paragraphe \ref{para:Tinit_slab}).
\item Conditions aux limites: les conditions précédentes ne changent pas, mais à $t=0$, on impose en plus un flux radiatif $\Phi_{r}$ du côté $x=0$. On considère qu'il est intégralement absorbé par la paroi.
\end{itemize}

Les développements qui suivent sont largement inspirés du livre {\it Heat Conduction, Second edition, 1993} (ref. \cite{Ozisik:1993}), et en particulier du chapitre 2-4.


\subsection{Profil de température initial}
\label{para:Tinit_slab}

Le profil de température initial $f(x)=T_{0}(x)=T(x,t=0)$ est la solution du problème suivant:

\begin{itemize}
\item Conduction stationnaire dans le mur:
\begin{equation}
\frac{d^{2} T_{0}(x)}{d x^{2}}=0
\end{equation}
\item Conditions aux limites:
\begin{equation}
-\lambda\frac{d T_{0}(x)}{dx}_{|_{x=0}}=h_{c1}\Bigl[T_{f1}-T_{0}(0)\Bigr]+h_{r1}\Bigl[T_{r1}-T_{0}(0)\Bigr]
\end{equation}
\begin{equation}
-\lambda\frac{d T_{0}(x)}{dx}_{|_{x=e}}=h_{c2}\Bigl[T_{0}(e)-T_{f2}\Bigr]+h_{r2}\Bigl[T_{0}(e)-T_{r2}\Bigr]
\end{equation}
\end{itemize}

Etant donné que $\frac{d T_{0}(x)}{dx}_{|_{x=0}}$=$\frac{d T_{0}(x)}{dx}_{|_{x=e}}$=$\frac{T_{0}(e)-T_{0}(0)}{e}$, les inconnues $T_{0}(0)$ et $T_{0}(e)$ sont les solutions du système linéaire suivant:
\begin{equation}
\begin{bmatrix}
    \lambda/e+h_{c1}+h_{r1} & -\lambda/e \\
    -\lambda/e & \lambda/e+h_{c2}+h_{r2} 
\end{bmatrix}
 \begin{bmatrix}
    T_{0}(0)  \\
    T_{0}(e)
\end{bmatrix}
=
 \begin{bmatrix}
   h_{c1}T_{f1}+h_{r1}T_{r1}  \\
   h_{c2}T_{f2}+h_{r2}T_{r2}
\end{bmatrix}
\end{equation}

La solution est:
\begin{equation}
f(x)=\frac{T_{0}(e)-T_{0}(0)}{e}x+T_{0}(0)
\end{equation}




\subsection{Superposition}

La solution du problème complet $T(x,t)$ peut s'exprimer comme:

\begin{equation}
T(x,t)=T_{1}(x,t)+T_{2}(x,t)
\end{equation}

Il s'agit de la superposition des solutions des deux problèmes élémentaires suivants:

\subsubsection{Sous-problème 1: stationnaire}
\label{para:sp1}
Le premier sous-problème est le problème de conduction stationnaire suivant:
\begin{itemize}
\item Conduction stationnaire dans le mur:
\begin{equation}
\frac{d^{2} T_{1}(x)}{d x^{2}}=0
\end{equation}
\item Conditions aux limites:
\begin{equation}
-\lambda\frac{d T_{1}(x)}{dx}_{|_{x=0}}=h_{c1}\Bigl[T_{f1}-T_{1}(0)\Bigr]+h_{r1}\Bigl[T_{r1}-T_{1}(0)\Bigr]+\Phi_{r}
\end{equation}
\begin{equation}
-\lambda\frac{d T_{1}(x)}{dx}_{|_{x=e}}=h_{c2}\Bigl[T_{1}(e)-T_{f2}\Bigr]+h_{r2}\Bigl[T_{1}(e)-T_{r2}\Bigr]
\end{equation}
\end{itemize}

Etant donné que $\frac{d T_{1}(x)}{dx}_{|_{x=0}}$=$\frac{d T_{1}(x)}{dx}_{|_{x=e}}$=$\frac{T_{1}(e)-T_{1}(0)}{e}$, les inconnues $T_{1}(0)$ et $T_{1}(e)$ sont les solutions du système linéaire suivant:
\begin{equation}
\begin{bmatrix}
    \lambda/e+h_{c1}+h_{r1} & -\lambda/e \\
    -\lambda/e & \lambda/e+h_{c2}+h_{r2} 
\end{bmatrix}
 \begin{bmatrix}
    T_{1}(0)  \\
    T_{1}(e)
\end{bmatrix}
=
 \begin{bmatrix}
   h_{c1}T_{f1}+h_{r1}T_{r1}+\Phi_{r}  \\
   h_{c2}T_{f2}+h_{r2}T_{r2}
\end{bmatrix}
\end{equation}

Une fois que les températures de paroi $T_{1}(0)$ et $T_{1}(e)$ sont connues, la solution du problème stationnaire est la suivante:
\begin{equation}
T_{1}(x)=\frac{T_{1}(e)-T_{1}(0)}{e}x+T_{1}(0)
\end{equation}

\subsubsection{Sous-problème 2: instationnaire homogène}
\label{para:sp2}
Le second sous-problème est le problème de condution instationnaire homogène suivant:
\begin{itemize}
\item Conduction instationnaire dans le mur:
\begin{equation}
\frac{\partial T_{2}(x,t)}{\partial t}=\alpha\frac{\partial^{2} T_{2}(x,t)}{\partial x^{2}}
\label{eq:diffusion1D}
\end{equation}
avec $\alpha=\frac{\lambda}{\rho C_{p}}$

\item Conditions aux limites:
\begin{equation}
\lambda\frac{\partial T_{2}(x,t)}{\partial x}_{|_{x=0}}=h_{1}T_{2}(0,t)
\label{eq:CL1}
\end{equation}
\begin{equation}
\lambda\frac{\partial T_{2}(x,t)}{\partial x}_{|_{x=e}}=-h_{2}T_{2}(e,t) 
\label{eq:CL2}
\end{equation}

avec $h_{1}=h_{c1}+h_{r1}$ et $h_{2}=h_{c2}+h_{r2}$

\item Condition initiale: $T_{2}(x,0)=F(x)=f(x)-T_{1}(x)$ avec $f(x)=\frac{T_{0}(e)-T_{0}(0)}{e}x+T_{0}(0)$ (voir paragraphe \ref{para:Tinit_slab}).
\end{itemize}


\subsection{Résolution du problème instationnaire homogène}

\subsubsection{Séparation des variables}

La solution au sous-problème 2 est cherchée par la méthode de séparation des variables, c'est à dire comme le produit d'une fonction de l'espace $X(x)$ et d'une fonction du temps $\Gamma(t)$. En fait, on se rend vite compte que la solution prend la forme d'une série de Fourier de la forme:

\begin{equation}
T_{2}(x,t)=\sum\limits_{m=1}^{+\infty} c_{m} X_{m}(x) \Gamma_{m}(t)
\label{eq:T2_gen_exp}
\end{equation}

Avec comme terme temporel:

\begin{equation}
\Gamma_{m}(t)=exp\Bigl(-\alpha \beta_{m}^{2} t\Bigr)
\end{equation}

Et les fonctions spatiales $X_{m}(x)$ sont cherchées sous la forme suivante:

\begin{equation}
X_{m}(x)=Acos(\beta_{m}x)+Bsin(\beta_{m}x)
\end{equation}

Afin de calculer les coefficients $c_{m}$ de la série de Fourier, on utilise la propriété d'orthogonalité de la base de fonctions sur laquelle les fonctions spatiales $X_{m}(x)$ sont exprimées, à partir du profil de température initial $F(x)$:
\begin{equation}
F(x)=T_{2}(x,t=0)=\sum\limits_{m=1}^{+\infty} c_{m}X_{m}(x)
\end{equation}

d'où:
\begin{equation}
\int_{0}^{e} F(x) X_{n}(x)dx=\sum\limits_{m=1}^{+\infty} c_{m}\int_{0}^{e} X_{m}(x) X_{n}(x)dx
\end{equation}

Le fait que la base de fonctions sur laquelle les fonctions $X_{m}(x)$ sont exprimées soit orthogonale se traduit par le fait que $\int_{0}^{e} X_{m}(x) X_{n}(x)dx=0$ pour $n \neq m$. On en déduit l'expression générale des coefficients $c_{m}$ de la série de Fourier en fonction de $F(x)$, le profil de température initial du problème instationnaire homogène, encore quelconque à ce stade:

\begin{equation}
c_{m}=\frac{1}{N(\beta_{m})}\int_{0}^{e} F(x) X_{m}(x)dx
\label{eq:cm}
\end{equation}

avec $N(\beta_{m})$ l'intégrale de normalisation:

\begin{equation}
N(\beta_{m})=\int_{0}^{e} X_{m}(x)^{2}dx
\end{equation}


\subsubsection{Les conditions aux limites}

Une fois que l'on injecte l'expression générale de $T_{2}(x,t)$ (\ref{eq:T2_gen_exp}) dans la première condition aux limites (\ref{eq:CL1}) en utilisant $X_{m}(x)=Acos(\beta_{m}x)+Bsin(\beta_{m}x)$, on obtient facilement le résultat suivant: $\lambda\beta_{m}B=h_{1} A$. On cherche donc maintenant des fonctions spatiales de la forme:
\begin{equation}
X_{m}(x)=\beta_{m}cos(\beta_{m}x)+H_{1}sin(\beta_{m}x)
\end{equation}

avec $H_{1}=\frac{h_{1}}{\lambda}$

La même opération sur la seconde condition aux limites (\ref{eq:CL2}) permet d'obtenir:

\begin{equation}
tan(\beta_{m}e)=\frac{\beta_{m}(H_{1}+H_{2})}{\beta_{m}^{2}-H_{1}H_{2}}
\label{eq:tan_relation}
\end{equation}

avec $H_{2}=\frac{h_{2}}{\lambda}$

La relation implicite précédente admet une infinité de racines $\beta_{m}$: il s'agit des valeurs propres des fonctions $X_{m}(x)$. Une procédure numérique permet d'obtenir leurs valeurs, à un ordre de développement donné (en pratique, les résultats présentés dans ce document ont été obtenus en développant la série de Fourier à l'ordre 10$^{4}$).


\subsubsection{Intégrale de normalisation}

On cherche maintenant à évaluer:
\begin{equation}
N(\beta_{m})=\int_{0}^{e} X_{m}(x)^{2}dx
\end{equation}

En utilisant le résultat précédent:
\begin{equation}
X_{m}(x)=\beta_{m}cos(\beta_{m}x)+H_{1}sin(\beta_{m}x)
\end{equation}

Donc:
\begin{equation}
\begin{split}
N(\beta_{m}) &=\int_{0}^{e} \Bigl[\beta_{m}cos(\beta_{m}x)+H_{1}sin(\beta_{m}x)\Bigr]^{2}dx \\
&=\beta_{m}^{2}\int_{0}^{e} cos^{2}(\beta_{m}x)dx+H_{1}^{2}\int_{0}^{e} sin^{2}(\beta_{m}x)dx+2H_{1}\beta_{m}\int_{0}^{e} cos(\beta_{m}x)sin(\beta_{m}x)dx
\end{split}
\end{equation}

On calcule séparément:

\begin{equation}
\int_{0}^{e} cos^{2}(\beta_{m}x)dx=\frac{e}{2}+\frac{sin(2\beta_{m}e)}{4\beta_{m}}
\end{equation}

\begin{equation}
\int_{0}^{e} sin^{2}(\beta_{m}x)dx=\frac{e}{2}-\frac{sin(2\beta_{m}e)}{4\beta_{m}}
\end{equation}

\begin{equation}
\int_{0}^{e} cos(\beta_{m}x)sin(\beta_{m}x)dx=\frac{sin^{2}(\beta_{m}e)}{2\beta_{m}}
\end{equation}

En substituant les résultats des trois intégrales précédentes dans la relation de $N(\beta_{m})$, et en utilisant en plus la relation \ref{eq:tan_relation} plus le fait que $sin(2\beta_{m}e)=2sin(\beta_{m}e)cos(\beta_{m}e)$, on finit par obtenir:

\begin{equation}
N(\beta_{m})=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\Bigl[\frac{1}{2}\frac{\beta_{m}^{2}-H_{1}^{2}}{\beta_{m}}\frac{\beta_{m}^{2}-H_{1}H_{2}}{\beta_{m}(H_{1}+H_{2})}+H_{1}\Bigr]sin^{2}(\beta_{m}e)
\label{eq:N2}
\end{equation}

A partir de la relation \ref{eq:tan_relation}, on peut également obtenir:

\begin{equation}
\begin{split}
sin^{2}(\beta_{m}e)& =\frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\beta_{m}^{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}+H_{2}^{2}\Bigr)+H_{1}^{2}H_{2}^{2}} \\
&=\frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)}
\end{split}
\label{eq:sin2_relation}
\end{equation}

En combinant \ref{eq:N2} et \ref{eq:sin2_relation}, on obtient:

\begin{equation}
\begin{split}
N(\beta_{m}) & =\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\Bigl[\frac{1}{2}\frac{\beta_{m}^{2}-H_{1}^{2}}{\beta_{m}}\frac{\beta_{m}^{2}-H_{1}H_{2}}{\beta_{m}(H_{1}+H_{2})}+H_{1}\Bigr] \frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)} \\
& =\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{\Bigl(\beta_{m}^{2}-H_{1}^{2}\Bigr)\Bigl(\beta_{m}^{2}-H_{1}H_{2}\Bigr)+2\beta_{m}^{2}H_{1}\Bigl(H_{1}+H_{2}\Bigr)}{2\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)} \frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)} \\
&=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{\beta_{m}^{4}+\beta_{m}^{2}H_{1}^{2}+\beta_{m}^{2}H_{1}H_{2}+H_{1}^{3}H_{2}}{2\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)} \frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)} \\
&=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{1}H_{2}\Bigr)}{2\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)} \frac{\beta_{m}^{2}\Bigl(H_{1}+H_{2}\Bigr)^{2}}{\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr) \Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)} \\
&=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{\Bigl(\beta_{m}^{2}+H_{1}H_{2}\Bigr)\Bigl(H_{1}+H_{2}\Bigr)}{2\Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)}\\
&=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{H_{1}\Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)+H_{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)}{2\Bigl(\beta_{m}^{2}+H_{2}^{2}\Bigr)}\\
&=\frac{e}{2}\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)+\frac{1}{2}\Biggl[\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)\frac{H_{2}}{\beta_{m}^{2}+H_{2}^{2}}+H_{1}\Biggr]\\
\end{split}
\label{eq:N3}
\end{equation}

Finalement:
\begin{equation}
N(\beta_{m})=\frac{1}{2}\Biggl[\Bigl(\beta_{m}^{2}+H_{1}^{2}\Bigr)\Bigl(e+\frac{H_{2}}{\beta_{m}^{2}+H_{2}^{2}}\Bigr) +H_{1}\Biggr]
\label{eq:Nbeta}
\end{equation}

\subsubsection{Coefficients de la série de Fourier}

Il ne reste qu'à intégrer la condition initiale $F(x)$ dans la relation \ref{eq:cm}: ici on va utiliser $F(x)=ax+b$ un profil linéaire, avec $a=\frac{T_{0}(e)-T_{0}(0)}{e}-\frac{T_{1}(e)-T_{1}(0)}{e}$ et $b=T_{0}(0)-T_{1}(0)$. On a donc:

\begin{equation}
\begin{split}
\int_{0}^{e} F(x) X_{m}(x)dx&=\int_{0}^{e} (ax+b)\Bigl(\beta_{m}cos(\beta_{m}x)+H_{1}sin(\beta_{m}x)\Bigr)\\
&=a\beta_{m}\int_{0}^{e}x cos(\beta_{m}x)dx+aH_{1}\int_{0}^{e}x sin(\beta_{m}x)dx\\
&+b\beta_{m}\int_{0}^{e} cos(\beta_{m}x)dx+bH_{1}\int_{0}^{e} sin(\beta_{m}x)dx
\end{split}
\label{eq:ic_integration}
\end{equation}

Avec:

\begin{equation}
\int_{0}^{e}x cos(\beta_{m}x)dx=\frac{e.sin(\beta_{m}e)}{\beta_{m}}+\frac{cos(\beta_{m}e)-1}{\beta_{m}^{2}}
\end{equation}

\begin{equation}
\int_{0}^{e}x sin(\beta_{m}x)dx=-\frac{e .cos(\beta_{m}e)}{\beta_{m}}+\frac{sin(\beta_{m}e)}{\beta_{m}^{2}}
\end{equation}

\begin{equation}
\int_{0}^{e} cos(\beta_{m}x)dx=\frac{sin(\beta_{m}e)}{\beta_{m}}
\end{equation}

\begin{equation}
\int_{0}^{e} sin(\beta_{m}x)dx=\frac{1-cos(\beta_{m}e)}{\beta_{m}}
\end{equation}

On obtient numériquement les valeurs des coefficients $c_{m}$ en combinant les relations \ref{eq:cm}, \ref{eq:ic_integration} et les quatre intégrales ci-dessus.


\subsection{Résultats}
\label{eq:resultsslab}


L'application numérique a été réalisée pour les valeurs suivantes:
\begin{itemize}
\item épaisseur du mur: $e$=0.10 $m$
\item coefficients d'échange convectif: $h_{c1}$=100 $W.m^{-2}.K^{-1}$, $h_{c2}$=100 $W.m^{-2}.K^{-1}$
\item coefficients d'échange radiatif linéarisé: $h_{r1}$=6 $W.m^{-2}.K^{-1}$, $h_{r2}$=4 $W.m^{-2}.K^{-1}$
\item densité du solide: $\rho$=1700 $kg.m^{-3}$
\item capacité calorifique du solide: $C_{p}$=800 $J.kg^{-1}.K^{-1}$
\item conductivité thermique du solide: $\lambda$=1.15 $W.m^{-1}.K^{-1}$
\item températures de fluide: $T_{f1}$=330 $K$, $T_{f2}$=290 $K$
\item flux radiatif imposé à partir de $t=0$: $\Phi_{r}$=10$^{4}$ $W.m^{-2}$
\item températures d'ambiance radiative: $T_{r1}$=320 $K$, $T_{r2}$=280 $K$
\end{itemize}

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid_analytical.pdf}
\caption[]{Profils de températures analytiques}
\label{fig:Tsolid_analytical}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid.pdf}
\caption[]{Profils de températures obtenus par l'algorithme de Monte-Carlo à l'aide de $10^{5}$ échantillons statistiques par position sonde}
\label{fig:Tsolid}
\end{figure}

La figure \ref{fig:Tsolid_analytical} présente les profils de température obtenus pour 11 positions dans le mur réparties uniformément entre 0 et $e$, et pour 11 valeurs du temps entre 0 et 6000 secondes (répartis de façon uniforme par pas de 600 secondes).



\begin{figure}[p]
\centering
\includegraphics[width=0.80\textwidth]{./figures/Te_signal_analytical.pdf}
\caption[]{Profil temporel de température en $x$=$e$ obtenu de façon analytique}
\label{fig:Te_signal_analytical}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=0.80\textwidth]{./figures/Te_signal.pdf}
\caption[]{Profil temporel de température en $x$=$e$ obtenu par l'algorithme de Monte-Carlo pour $10^{5}$ réalisations statistiques par position.}
\label{fig:Te_signal}
\end{figure}

Les mêmes résultats ont été dans un second temps obtenus par un algorithme de Monte-Carlo, en utilisant $10^{5}$ réalisations statistiques par position sonde ($x_{0}$/$t_{0}$): ils sont présentés sur la figure \ref{fig:Tsolid}. Les deux séries de courbes sont quasiment indiscernables, les écart-types numériques obtenus étant très faibles.



Les figures \ref{fig:Te_signal_analytical} et \ref{fig:Te_signal} représentent le profil temporel de température obtenu en $x$=$e$, respectivement de façon analytique et par l'algorithme de Monte-Carlo. 

Là encore les résultats se superposent, aux incertitudes statistiques près.

Ce profil temporel de température obtenu en face non chauffée est intéressant, car il apporte une information sur la dynamique temporelle, et surtout ce signal est mesurable (caméra IR). Reste à voir comment l'utiliser pour inverser une propriété physique du matériau, par exemple la conductivité.


\subsection{Conditions de type Dirichlet}

Reprenons le problème précédent, où seulent les conditions aux limites changent: on impose maintenant la valeur de la température des deux parois:
\begin{itemize}
\item pour $t \le 0$ les températures de paroi sont imposées à des valeurs initiales ($T_{0}(0)$ et $T_{0}(e)$), d'où découle le profil de température initial dans le solide: $f(x)=\frac{T_{0}(e)-T_{0}(0)}{e}x+T_{0}(0)$.
\item pour $t  > 0$ la température de la paroi de gauche est augmentée de $\Delta T_{b}=$, tandis que la température de la paroi de droite est maintenue.
\end{itemize}

La résolution s'effectue de façon similaire au cas précédent: on résoud successivement:
\begin{enumerate}
\item le sous-problème stationnaire: inutile cependant de calculer les températures des parois au stationnaire par résolution d'un système linéaire comme en partie \ref{para:sp1}, puisque ces températures sont connues (resp. $T_{1}(0)=T_{0}(0)+\Delta T_{b}K$ et $T_{1}(e)=T_{0}(e)$). Le profil de température stationnaire dans le mur suit une fonction linéaire $T_{1}(x)=\frac{T_{1}(e)-T_{1}(0)}{e}x+T_{1}(0)$.
\item le sous-problème instationnaire homogène suivant:
\begin{itemize}
\item Conduction instationnaire dans le mur:
\begin{equation}
\frac{\partial T_{2}(x,t)}{\partial t}=\alpha\frac{\partial^{2} T_{2}(x,t)}{\partial x^{2}}
\end{equation}
avec $\alpha=\frac{\lambda}{\rho C_{p}}$

\item Conditions aux limites:
\begin{equation}
T_{2}(0,t)=0
\end{equation}
\begin{equation}
T_{2}(e,t)=0
\end{equation}

\item Condition initiale: $T_{2}(x,0)=F(x)=f(x)-T_{1}(x)$ avec $f(x)=\frac{T_{0}(e)-T_{0}(0)}{e}x+T_{0}(0)$.
\end{itemize}
\end{enumerate}

Le sous-problème instationnaire homogène se résoud, comme en première partie, par séparation de variables:
\begin{equation}
T_{2}(x,t)=\sum\limits_{m=1}^{+\infty} c_{m} X_{m}(x) \Gamma_{m}(t)
\end{equation}

Les fonctions de l'espace $X_{m}(x)$ s'expriment comme $A.cos(\beta_{m}x)+B.sin(\beta_{m}x)$. La première condition aux limites permet de trouver $A=0$ et $B=1$; la seconde condition aux limites permet d'obtenir $\beta_{m}=\frac{m\pi}{e}$. On a donc:
\begin{equation}
T_{2}(x,t)=\sum\limits_{m=1}^{+\infty} c_{m} sin\Bigl(\frac{m\pi}{e}x\Bigr) exp\Bigl[-\alpha\Bigl(\frac{m\pi}{e}\Bigr)^{2}t\Bigr]
\end{equation}

Les coefficients de la série de Fourier s'expriment, comme précédemment, à partir des conditions initiales:
\begin{equation}
c_{m}=\frac{1}{N(\beta_{m})}\int_{0}^{e} F(x) X_{m}(x)dx
\end{equation}

avec:
\begin{equation}
N(\beta_{m})=\int_{0}^{e} X_{m}(x)^{2}dx
\end{equation}

On obtient assez facilement:

\begin{equation}
N(\beta_{m})=\frac{e}{2}
\end{equation}

\begin{equation}
c_{m}=-\frac{2\Delta T_{b}}{m\pi}
\end{equation}

Les figures \ref{fig:Tsolid_analytical_Dirichlet} et \ref{fig:Tsolid_Dirichlet} présentent les profils de température obtenus pour différentes valeurs du temps, respectivement de façon analytique et par l'algorithme de Monte-Carlo, à l'aide de $10^{4}$ réalisations statistiques par position sonde (d'où une variance plus importante que dans la figure \ref{fig:Tsolid}).

L'application numérique a été réalisée pour le même matériau qu'en partie \ref{eq:resultsslab} , les valeurs initiales des températures des parois étant:  $T_{0}(0)=$310 K, $T_{0}(e)=$290 K, et l'augmentation de température sur la face $x=0$ étant $\Delta T_{b}=$20 K.

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid_analytical_Dirichlet.pdf}
\caption[]{Profils de températures analytiques}
\label{fig:Tsolid_analytical_Dirichlet}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid_Dirichlet.pdf}
\caption[]{Profils de températures obtenus par l'algorithme de Monte-Carlo à l'aide de $10^{4}$ échantillons statistiques par position sonde}
\label{fig:Tsolid_Dirichlet}
\end{figure}

\newpage

\section{Géométrie sphérique}

\subsection{Equation de la chaleur en coordonnées sphérique}

L'équation de la diffusion de la chaleur s'exprime de façon générale:

\begin{equation}
\frac{\partial T(x,t)}{\partial t}=\alpha \nabla T(x,t)
\end{equation}

avec $\nabla$ l'opérateur Laplacien: $\nabla(f)=div(\overrightarrow{grad}(f))$

Utilisons un système de coordonnées sphériques où une position $P$ est repérée par ses coordonnées $(r,\theta,\phi)$ où $\theta$ est repéré entre la verticale et la direction courante ($\theta \in [0,\pi]$) et $\phi$ est repéré entre l'axe $(Ox)$ et la projection de la direction courante sur le plan $(0xy)$ ($\phi \in [0,2\pi]$). Le repère local $(\overrightarrow{e_{r}},\overrightarrow{e_{\theta}},\overrightarrow{e_{\phi}}$) est défini de la façon suivante:
\begin{itemize}
\item $\overrightarrow{e_{r}}$ est le vecteur unitaire dans la direction $\overrightarrow{OP}$
\item $\overrightarrow{e_{\theta}}$ est le vecteur unitaire tangent en $P$ au cerle de rayon $r$, obtenu lorsque $\theta$ varie sur $[0,2\pi]$ (à $\phi$ constant) dirigé dans le sens obtenu par une variation positive de $\theta$. En d'autres termes $\overrightarrow{e_{\theta}}=\frac{\overrightarrow{\partial OP}}{\partial \theta}$
\item $\overrightarrow{e_{\phi}}$ est le vecteur unitaire qui complète le trièdre direct: $\overrightarrow{e_{\phi}}=\overrightarrow{e_{r}} \land \overrightarrow{e_{\theta}}$
\end{itemize}

Dans ces conditions, on a:
\begin{itemize}
\item Le gradient d'une fonction $f(r,\theta,\phi)$, décomposé sur la base $(\overrightarrow{e_{r}},\overrightarrow{e_{\theta}},\overrightarrow{e_{\phi}}$):
\begin{equation}
\overrightarrow{grad}(f)=\frac{\partial f}{\partial r}\overrightarrow{e_{r}}+\frac{1}{r}\frac{\partial f}{\partial \theta}\overrightarrow{e_{\theta}}+\frac{1}{r sin(\theta)}\frac{\partial f}{\partial \phi}\overrightarrow{e_{\phi}}
\end{equation}
\item La divergence d'un vecteur $\overrightarrow{u}=[u_{r},u_{\theta},u_{\phi}]$:
\begin{equation}
div(\overrightarrow{u})=\frac{1}{r^{2}}\frac{\partial}{\partial r}\Bigl(r^{2} u_{r}\Bigr)+\frac{1}{r sin(\theta)}\frac{\partial}{\partial \theta}\Bigl(sin(\theta) u_{\theta}\Bigr) +\frac{1}{r sin(\theta)}\frac{\partial}{\partial \phi}\Bigl(u_{\phi}\Bigr)
\end{equation}
\end{itemize}

L'équation de la chaleur s'exprime donc, de façon générale:
\begin{equation}
\frac{1}{\alpha}\frac{\partial T(x,t)}{\partial t}=\frac{1}{r^{2}}\frac{\partial}{\partial r}\Bigl(r^{2} \frac{\partial T(x,t)}{\partial r} \Bigr)+\frac{1}{r^{2} sin(\theta)}\frac{\partial}{\partial \theta}\Bigl(sin(\theta) \frac{\partial T(x,t)}{\partial \theta}\Bigr)+\frac{1}{r^{2} sin^{2}(\theta)}\frac{\partial}{\partial \phi}\Bigl(\frac{\partial T(x,t)}{\partial \phi}\Bigr)
\end{equation}

{\bf Hyp.}: si la fonction $T(x,t)$ est à symétrie sphérique, c'est à dire qu'elle ne dépend spatialement que du rayon: $T(x,t)=T(r,t)$ ($\frac{\partial}{\partial \theta}=\frac{\partial}{\partial \phi}=0$), alors l'équation de la chaleur s'exprime:
\begin{equation}
\frac{1}{\alpha}\frac{\partial T(r,t)}{\partial t}=\frac{1}{r^{2}}\frac{\partial}{\partial r}\Bigl(r^{2} \frac{\partial T(r,t)}{\partial r} \Bigr)=\frac{2}{r}\frac{\partial T(r,t)}{\partial r}+\frac{\partial^{2} T(r,t)}{\partial r^{2}}
\end{equation}

Une astuce consiste ici à remarquer que $\frac{\partial^{2}}{\partial r^{2}}\Bigl(r T(r,t)\Bigr)=\frac{2}{r}\frac{\partial T(r,t)}{\partial r}+\frac{\partial^{2} T(r,t)}{\partial r^{2}}$; on obtient donc, dans l'hypothèse de symétrie sphérique:
\begin{equation}
\frac{1}{\alpha}\frac{\partial T(r,t)}{\partial t}=\frac{1}{r}\frac{\partial^{2}}{\partial r^{2}}\Bigl(r T(r,t) \Bigr)
\end{equation}
Nous allons conserver cette hypothèse tout au long des développements qui viennent.

\subsection{Problème}

Nous considérons le problème de la conduction thermique en géométrie sphérique, sous l'hypothèse de symétrie sphérique, c'est à dire que la température en tout point d'une boule ne dépend que de la distance au centre de la boule, et du temps:

\begin{equation}
\frac{1}{\alpha}\frac{\partial T(r,t)}{\partial t}=\frac{1}{r}\frac{\partial^{2}}{\partial r^{2}}\Bigl(r T(r,t) \Bigr)
\end{equation}
pour $r \in [0,R]$

La condition initiale est uniforme: la température initiale dans la boule est uniforme, à une température $f(r)=T_{0}$

La condition aux limites est de troisième espèce, c'est à dire que l'échange thermique s'opère en paroi par convection et/ou par rayonnement (linéarisé):
\begin{equation}
-\frac{\partial T(r,t)}{\partial r}_{|r=R}=h_{c}\Bigl[T(R,t)-T_{f}\Bigr]+h_{r}\Bigl[T(R,t)-T_{r}\Bigr]
\end{equation}
avec $h_{c}$ le coefficient d'échange par convection avec un fluide de température $T_{f}$ et $h_{r}$ le coefficient d'échange par rayonnement linéarisé, avec une température d'ambiance radiative uniforme $T_{r}$.

\subsection{Résolution}

On utilise une nouvelle fois la superposition: la température $T(r,t)$ est la somme de $T_{1}(r,t)$, solution du sous-problème stationnaire, et de $T_{2}(r,t)$, solution du sous-problème instationnaire homogène.

\subsubsection{Sous-problème stationnaire}

En régime stationnaire, la température dans la boule est homogène. Elle est notamment égale à la température de paroi (uniforme) qui ne varie plus au cours du temps. On en déduit, à partir de la condition aux limites appliquée pour $\frac{\partial T(r,t)}{\partial r}_{|r=R}=0$:

\begin{equation}
T_{1}=\frac{h_{c}T_{f}+h_{r}T_{r}}{h_{c}+h_{r}}
\end{equation}

\subsubsection{Sous-problème instationnaire homogène}

Le problème est le suivant: 
\begin{equation}
\frac{1}{\alpha}\frac{\partial T_{2}(r,t)}{\partial t}=\frac{1}{r}\frac{\partial^{2}}{\partial r^{2}}\Bigl(r T_{2}(r,t) \Bigr)
\end{equation}
pour $r \in [0,R]$

\begin{itemize}
\item Condition initiale: $F(r)=f(r)-T_{1}=T_{0}-T_{1}$
\item Condition aux limites:
\begin{equation}
-\frac{\partial T_{2}(r,t)}{\partial r}_{|r=R}=(h_{c}+h_{r})T_{2}(R,t)
\end{equation}
Soit $h=h_{c}+h_{r}$ le coefficient d'échange global.
\end{itemize}

La résolution s'effectue par la technique de séparation des variables. Mais un changement de variables astucieux préalable permet de ramener l'équation de la diffusion de la chaleur à la forme bien connue de l'équation \ref{eq:diffusion1D}: soit la fonction $u(r,t)=r T_{2}(r,t)$. On en déduit que le problème devient:

\begin{equation}
\frac{1}{\alpha}\frac{\partial u(r,t)}{\partial t}=\frac{\partial^{2}}{\partial r^{2}}\bigl(u(r,t) \bigr)
\end{equation}
pour $r \in [0,R]$

\begin{itemize}
\item Condition initiale: $u(r,t=0)=r F(r)=r \bigl[T_{0}-T_{1}\bigr]$
\item Condition aux limites:
\begin{equation}
\frac{\partial u(r,t)}{\partial r}_{|r=R}=\Bigl(\frac{1}{R}-h\Bigr)u(R,t)
\end{equation}
Avec $R$ le rayon de la boule et $h=h_{c}+h_{r}$ le coefficient d'échange global.
\end{itemize}

Il est maintenant facile d'appliquer la technique de séparation des variables; on a donc:
\begin{equation}
u(r,t)=\sum\limits_{m=1}^{+\infty} c_{m}X_{m}(r)\Gamma(t)
\end{equation}

\begin{equation}
  \begin{cases}
    \Gamma(t)=exp\bigl(-\alpha\beta_{m}^{2}t\bigr)\\
    X_{m}(r)=A cos(r \beta_{m})+B sin(r \beta_{m})
  \end{cases}
\end{equation}

La condition aux limites permet de trouver $A=0$ et $B=1$, d'où $X_{m}(r)=sin(r \beta_{m})$

Afin de trouver une relation permettant de calculer les coefficients $\beta_{m}$, il nous faut une autre relation. On remarque donc, que dans le cas de la fonction $u(r,t)=r T(r,t)$, la valeur de $u$ pour $r=0$ est toujours nulle (quel que soit $t$). A partir de là, on trouve:
\begin{equation}
tan(R \beta_{m})=\frac{R \beta_{m}}{1-h R}
\label{eq:tr2}
\end{equation}

Les coefficients $c_{m}$ de la série de Fourier s'expriment comme:
\begin{equation}
c_{m}=\frac{1}{N(\beta_{m})}\int_{0}^{R} r F(r) X_{m}(r)  dr
\end{equation}

avec $N(\beta_{m})$ l'intégrale de normalisation:
\begin{equation}
N(\beta_{m})=\int_{0}^{R} X_{m}^{2}(r) dr
\end{equation}

On trouve facilement:
\begin{equation}
N(\beta_{m})=\frac{R}{2}-\frac{sin(2 R \beta_{m})}{4 \beta_{m}}
\end{equation}

\begin{equation}
c_{m}=\frac{h\bigl[T_{0}-T_{1}\bigr] R^{2} cos(R \beta_{m})}{N(\beta_{m}) \beta_{m} \bigl(1-h R\bigr)}
\label{eq:cmspherical}
\end{equation}

\subsubsection{Résultats}

Il faut se souvenir que l'on vient de résoudre le problème sur la fonction $u(r,t)$; afin de trouver $T_{2}(r,t)$, il faut appliquer le changement de variable en sens inverse: $T_{2}(r,t)=\frac{u(r,t)}{r}$; la solution du problème sur $T(r,t)$ est donc finalement:

\begin{equation}
T(r,t)=\frac{h_{c}T_{f}+h_{r}T_{r}}{h_{c}+h_{r}}+\frac{1}{r}\sum\limits_{m=1}^{+\infty} c_{m} sin(r \beta_{m}) exp\bigl(-\alpha\beta_{m}^{2}t\bigr)
\label{eq:solspherical}
\end{equation}

où les valeurs propres $\beta_{m}$ sont les racines de l'équation implicite \ref{eq:tr2}, et où les coefficients $c_{m}$ de la série sont donnés par la relation \ref{eq:cmspherical}.

La résolution de l'équation \ref{eq:tr2} en particulier pose demande un peu d'attention: en effet, la présence de la fonction tangente impose l'existence de valeurs de $\beta_{m}$ pour lequelles la fonction dont il faut trouver les racines est indéfinie. Il se trouve que plus l'ordre des racines ($m$) augmente, et plus la valeur des racines $\beta_{m}$ est proche de ces singularités, ce qui rend leur détection très difficile.

Finalement, on peut remarquer que l'évaluation numérique de la relation \ref{eq:solspherical} se passe mal pour $r=0$. Il suffit de réaliser un développement limité de $sin(r \beta_{m})$ pour la limite $r \beta_{m} \rightarrow 0$ pour lever cette difficulté:

\begin{equation}
\lim_{r \beta_{m} \rightarrow 0} T(r,t)=\frac{h_{c}T_{f}+h_{r}T_{r}}{h_{c}+h_{r}}+\frac{1}{r}\sum\limits_{m=1}^{+\infty} \frac{h\bigl[T_{0}-T_{1}\bigr] R^{2} cos(R \beta_{m})}{N(\beta_{m}) \beta_{m} \bigl(1-h R\bigr)} \Bigl[r \beta_{m}-\frac{r^{3} \beta_{m}^{3}}{6}\Bigr] exp\bigl(-\alpha\beta_{m}^{2}t\bigr)
\end{equation}

D'où:
\begin{equation}
T(r=0,t)=\frac{h_{c}T_{f}+h_{r}T_{r}}{h_{c}+h_{r}}+\sum\limits_{m=1}^{+\infty} \frac{h\bigl[T_{0}-T_{1}\bigr] R^{2} cos(R \beta_{m})}{N(\beta_{m}) \bigl(1-h R\bigr)} exp\bigl(-\alpha\beta_{m}^{2}t\bigr)
\end{equation}

Les figures \ref{fig:Tsolid_analytical_spherical} et \ref{fig:Tsolid_spherical} présentent les profils de température radiaux obtenus pour diverses valeurs du temps, respectivement de façon analytique et par la méthode de Monte-Carlo à l'aide de $10^{4}$ réalisations statistiques par position sonde. Le matériau est identique à celui utilisé dans les applications numériques précédentes, et les conditions suivantes ont été utilisées: sphère de rayon $R=$0.10 m, coefficient d'échange convectif $h_{c}=$100 $W/m^{2}/K$, coefficient d'échange radiatif $h_{r}=$6 $W/m^{2}/K$, température initiale $T_{0}=$300 K, température d'air $T_{f}=$330 K, température d'ambiance radiative $T_{r}=$300 K (d'où $T_{1}=$300 K).

Les calculs analytiques ont été réalisés en poussant l'évaluation de la série de Fourier jusqu'à l'ordre $10^{4}$. Plus la série de Fourier est évaluée pour un ordre important, et plus les valeurs de la température pour $t=0$ seront proches de la condition initiale. Cependant, quelque soit l'ordre où la série de Fourier est évaluée, il reste un écart (de plus en plus infime) entre le profil de température évalué pour $t=0$ et la condition initiale, ce qui conduit à des erreurs numériques importantes lorsqu'on souhaite évaluer par exemple $\frac{\partial T(r,t)}{\partial t}_{|r=0,t=0}$ (valeur qui, d'un point de vue physique, est nulle). Ce problème peut être levé en utilisant d'une part l'information sur la condition initiale (plutôt que de tenter d'évaluer numériquement $T(r,t=0)$ à l'aide d'une série d'ordre infini), et d'autre part en utilisant un pas de temps pas trop faible.

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid_analytical_spherical.pdf}
\caption[]{Profils de températures analytiques}
\label{fig:Tsolid_analytical_spherical}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics[width=1.0\textwidth]{./figures/Tsolid_spherical.pdf}
\caption[]{Profils de températures obtenus par l'algorithme de Monte-Carlo à l'aide de $10^{4}$ échantillons statistiques par position sonde}
\label{fig:Tsolid_spherical}
\end{figure}


\newpage
\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}
