\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}G\IeC {\'e}om\IeC {\'e}trie slab}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Probl\IeC {\`e}me}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Profil de temp\IeC {\'e}rature initial}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Superposition}{4}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Sous-probl\IeC {\`e}me 1: stationnaire}{4}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Sous-probl\IeC {\`e}me 2: instationnaire homog\IeC {\`e}ne}{5}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}R\IeC {\'e}solution du probl\IeC {\`e}me instationnaire homog\IeC {\`e}ne}{5}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}S\IeC {\'e}paration des variables}{5}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Les conditions aux limites}{6}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Int\IeC {\'e}grale de normalisation}{6}{subsubsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.4}Coefficients de la s\IeC {\'e}rie de Fourier}{8}{subsubsection.2.4.4}
\contentsline {subsection}{\numberline {2.5}R\IeC {\'e}sultats}{9}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Conditions de type Dirichlet}{12}{subsection.2.6}
\contentsline {section}{\numberline {3}G\IeC {\'e}om\IeC {\'e}trie sph\IeC {\'e}rique}{15}{section.3}
\contentsline {subsection}{\numberline {3.1}Equation de la chaleur en coordonn\IeC {\'e}es sph\IeC {\'e}rique}{15}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Probl\IeC {\`e}me}{16}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}R\IeC {\'e}solution}{16}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Sous-probl\IeC {\`e}me stationnaire}{16}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Sous-probl\IeC {\`e}me instationnaire homog\IeC {\`e}ne}{16}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}R\IeC {\'e}sultats}{18}{subsubsection.3.3.3}
