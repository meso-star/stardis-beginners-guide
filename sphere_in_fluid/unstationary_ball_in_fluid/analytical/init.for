      subroutine init()
      implicit none
      include 'max.inc'
c
c     Purpose: initialization tasks
c
c     Input:
c
c     I/O
c     temp
      character*(Nchar_mx) command
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      command='rm -f ./coefficients_log.txt'
      call exec(command)
      command='rm -f ./roots_log.txt'
      call exec(command)
      command='rm -f ./Tsolid_initial.txt'
      call exec(command)
      command='rm -f ./Tsolid_stationary.txt'
      call exec(command)
      command='rm -rf ./Tsolid_analytical_t*'
      call exec(command)
      command='rm -rf ./Tsolid_analytical_x*'
      call exec(command)
      
      return
      end
      
