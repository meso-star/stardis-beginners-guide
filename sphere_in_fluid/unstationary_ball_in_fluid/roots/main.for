      program main
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: setting the numerical scheme for solving impliti equation of beta
c     
c     Variables
      character*(Nchar_mx) data_file
      integer dim
      logical mc
      integer Nterms
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
      character*(Nchar_mx) resfile
      double precision h,radius
      double precision x1,x2,dx,x
c     functions
      double precision fbeta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='program main'

      dim=3
      data_file='./data.in'
      call read_data(data_file,dim,
     &     R,hc,hr,epsilon,rho_s,Cp_s,lambda_s,
     &     Text,Trad,Tinitial,
     &     bfactor,sfactor,Nterms,mc,
     &     Nevent,Nprobe,probe_positions,Ntime,time_positions)

      call init()
c      call analytical_results(dim,Nprobe,probe_positions,
c     &     Ntime,time_positions,Nterms,
c     &     Tunstationary,Tinit,Tstationary)
c
      resfile='./fbeta.txt'
c      
      h=(hc+hr)/lambda_S
      radius=R
c     
      x1=0
      x2=100
      dx=1.0D-3
      x=x1
      
      open(11,file=resfile(1:strlen(resfile)))
      do while (x.le.x2)
         write(11,*) x,fbeta(radius,h,x)
         x=x+dx
      enddo ! while x<=x2
      close(11)
      write(*,*) 'Results file was produced:'
      write(*,*) resfile(1:strlen(resfile))
      
      end
      
