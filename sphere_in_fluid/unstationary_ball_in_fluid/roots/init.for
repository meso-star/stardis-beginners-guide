      subroutine init()
      implicit none
      include 'max.inc'
c
c     Purpose: initialization tasks
c
c     temp
      character*(Nchar_mx) command
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      command='rm -rf ./results/Tsolid_t*'
      call exec(command)
      command='rm -rf ./results/Tsolid_x*'
      call exec(command)
      command='rm -rf ./results/Tsolid_analytical_t*'
      call exec(command)
      command='rm -rf ./results/Tsolid_analytical_x*'
      call exec(command)
      
      return
      end
      
