      subroutine analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to evaluate analytical temperature fields
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c       + Nterms: number of terms in the Fourier series expansion
c     
c     Output:
c       + Tunstationary: temperature profile at each (x/t) positions
c       + Tinit: initial temperature profile
c       + Tstationary: stationary (final) temperature profile
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Np_mx)
      integer Nterms
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     temp
      integer n
      integer itime,iprobe,iterm
      double precision time,probe_x(1:Nvec_mx)
      double precision dbeta
      logical check,possible_root,keep_looking
      double precision max_err
      double precision Ts1,Ts2
      double precision x(1:Nvec_mx)
      double precision k
      double precision norm,norm_theoric
      double precision coeffa,coeffb
      double precision coeff(1:Nterms_mx)
      character*(Nchar_mx) str
      integer err
c     temp
      integer i,j
      integer Ndiscontinuity
      double precision betad
      logical debug
      double precision Tb1,Tb2,Ts_analytic
      double precision rad,theta,phi
      double precision radius,h
      double precision beta,beta1,beta2
      double precision f,f1,f2
      double precision errx,errf
      logical solution_found
      double precision beta0
      integer Np
      integer Nroots
      double precision root(1:Nterms_mx)
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
      logical keep_searching
      double precision dx
      integer Niter,Nstep,Nstep_mx
c     functions
      double precision fbeta
c     parameters
      double precision epsilon_theta
      parameter(epsilon_theta=1.0D-5) ! rad
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine analytical_results'

      Nstep_mx=1000000

c     Initial temperature profile
      open(11,file='./results/Tsolid_initial.txt')
      write(11,10) 'Temperatures @ intial conditions'
      write(11,*)
      do i=1,Nprobe
         Ts_analytic=Tinitial
         write(11,*) probe_positions(i,1),Ts_analytic
         Tinit(i)=Ts_analytic
      enddo                     ! i
      close(11)
c     Stationary temperature profile
      open(11,file='./results/Tsolid_stationary.txt')
      write(11,10) 'Temperatures @ stationary'
      write(11,*)
      write(11,10) 'Temperature on the left boundary:'
      write(11,*) Tb1
      write(11,10) 'Temperature on the right boundary:'
      write(11,*) Tb2
      write(11,*)            
      do i=1,Nprobe
         Ts_analytic=(hc*Text+hr*Trad)/(hc+hr)
         write(11,*) probe_positions(i,1),Ts_analytic
         Tstationary(i)=Ts_analytic
      enddo                     ! i
      close(11)

c     Solving T1, the unstationary homogeneous problem
      beta1=0.0D+0
      dbeta=1.0D-2
      h=(hc+hr)/lambda_s
      radius=R
      f1=fbeta(radius,h,beta1)
      errx=1.0D-8
      errf=1.0D-8
      Nroots=0
      Nstep=0
c     First discontinuity: get betad
      Ndiscontinuity=1
      call beta_discontinuity(Ndiscontinuity,radius,h,betad)
c     Debug
c      write(*,*) 'betad(',Ndiscontinuity,')=',betad
c     Debug
      keep_looking=.true.
      do while (keep_looking)
 111     continue
         beta2=beta1+dbeta
         Nstep=Nstep+1
         if ((beta1.le.betad).and.(beta2.ge.betad)) then
            dx=1.0D-3
            keep_searching=.true.
            Niter=0
            do while (keep_searching)
               Niter=Niter+1
               beta1=betad+dx
               f1=fbeta(radius,h,beta1)
               if (f1.lt.0.0D+0) then
                  keep_searching=.false.
               else
                  dx=dx/1.0D+1
               endif
               if (Niter.gt.Niter_mx) then
                  keep_searching=.false.
               endif
            enddo               ! while (keep_searching)
            Ndiscontinuity=Ndiscontinuity+1
            call beta_discontinuity(Ndiscontinuity,radius,h,betad)
c     Debug
c            write(*,*) 'betad(',Ndiscontinuity,')=',betad
c     Debug
            goto 111
         endif                  ! beta1<betad<beta2
         f2=fbeta(radius,h,beta2)
c     Debug
c         if ((Nroots.eq.417).and.
c     &        (beta2.gt.13116.0D+0).and.(beta2.lt.13117.0D+0)) then
c            write(*,*) beta1,f1,beta2,f2
c         endif
c     Debug
c     disriminate possible roots
         possible_root=.false.
         if (f1*f2.lt.0.0D+0) then
c            if (dabs(f1*f2).lt.1.0D+4) then
c               possible_root=.true.               
c            endif
            possible_root=.true.
         endif
c     
         if (possible_root) then
            call solve_fbeta_root(radius,h,beta1,beta2,errx,errf,
     &           solution_found,beta0)
            if (solution_found) then
               Nroots=Nroots+1
               if (Nroots.gt.Nterms_mx) then
                  call error(label)
                  write(*,*) 'Nroots=',Nroots
                  write(*,*) '> Nterms_mx=',Nterms_mx
                  stop
               else
                  root(Nroots)=beta0
                  Nstep=0
c     Debug
                  write(*,*) 'root(',Nroots,')=',root(Nroots)
c     Debug
c     Stop when enough terms have been found
                  if (Nroots.ge.Nterms) then
                     keep_looking=.false.
                  endif
               endif
            else
               write(*,*) 'no solution found in interval: [',
     &              beta1,'-',beta2,']'
            endif               ! solution_found
         endif                  ! possible_root
         beta1=beta2
         f1=f2
         if (Nstep.gt.Nstep_mx) then
            beta1=root(Nroots)
            f1=fbeta(radius,h,beta1)
            dbeta=dbeta/1.0D+1
            Nstep=0
         endif
      enddo                     ! while (keep_looking)
c
c     Generate log
      open(11,file='./results/roots_log.txt')
      write(11,*) 'Number of roots found:',Nroots
      if (Nroots.gt.0) then
         do i=1,Nroots
            write(11,*) 'root(',i,')=',root(i),fbeta(radius,h,root(i))
            k=h-1.0D+0/R
            norm_theoric=0.50D+0*(R*(root(i)**2.0D+0+k**2.0D+0)+k)
     &           /(root(i)**2.0D+0+k**2.0D+0)
            norm=R/2.0D+0-dsin(2.0D+0*root(i)*R)/(4.0D+0*root(i))
            write(11,*) 'norm_theoric=',norm_theoric,' norm=',norm
         enddo                  ! i
      endif
      close(11)
      
c     
c     Evaluation of the Fourier series coefficients
      coeffa=Tinitial-(hc*Text+hr*Trad)/(hc+hr)
      do i=1,Nterms
         norm=R/2.0D+0-dsin(2.0D+0*root(i)*R)/(4.0D+0*root(i))
         coeff(i)=(coeffa*(dsin(root(i)*R)/root(i)-R*dcos(root(i)*R))
     &        /root(i))/norm
      enddo                     ! i
c     Generate log
      open(11,file='./results/coefficients_log.txt')
      do i=1,Nterms
         write(11,*) coeff(i)
      enddo                     ! i
      close(11)
c     
      do itime=1,Ntime
c     write(*,*) 'time index: ',itime,' /',Ntime
         time=time_positions(itime)
         do iprobe=1,Nprobe
            do j=1,dim
               probe_x(j)=probe_positions(iprobe,j)
            enddo               ! j
            call cart2spher(probe_x(1),probe_x(2),probe_x(3),
     &           rad,theta,phi)
            if (time.le.0.0D+0) then ! time = 0
               Ts_analytic=Tinitial
            else                ! time > 0
c     Initialization: solution of stationary problems #1 and #2
               Ts1=(hc*Text+hr*Trad)/(hc+hr)
c     Fourier series evaluation
               Ts2=0.0D+0
               do iterm=1,Nterms
                  if ((root(iterm)*rad).lt.epsilon_theta) then
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *root(iterm)
     &                    *(1.0D+0-((root(iterm)*rad)**2.0D+0)/6.0D+0) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
                  else
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *dsin(root(iterm)*rad) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
     &                    /rad  ! variable change u(r,t)=r.T(r,t)
                  endif
               enddo            ! iterm
               Ts_analytic=Ts1+Ts2
            endif               ! time = 0 or > 0
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
            call num2str(itime,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'itime=',itime
               stop
            else
               results_file_t='./results/Tsolid_analytical_t'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     + space files
            call num2str(iprobe,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'iprobe=',iprobe
               stop
            else
               results_file_t='./results/Tsolid_analytical_x'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     --------------------------------------------------------------------------------------
         enddo                  ! iprobe
         close(11)
      enddo                     ! itime
      
      return
      end



      subroutine solve_fbeta_root(radius,h,beta1,beta2,errx,errf,
     &     solution_found,x0)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a root of f(beta)=0 between two known boundaries
c     
c     Input:
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c       + m: index of the period
c       + beta1: lower beta boundary
c       + beta2: lower beta boundary
c       + errx: maximum error over x (beta)
c       + errf: maximum error over f
c    
c    Output:
c      + solution_found: true if a solution has been found
c      + x0: f(x0)=0
c    
c    I/O
      double precision radius,h
      double precision beta1,beta2
      double precision errx,errf
      logical solution_found
      double precision x0
c     temp
      double precision x1,x2,x_mid
      double precision f1,f2,f_mid
      integer iter
      logical keep_running
c     functions
      double precision fbeta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine solve_fbeta_root'

      x1=beta1
      x2=beta2
      f1=fbeta(radius,h,x1)
      f2=fbeta(radius,h,x2)
      keep_running=.true.
      iter=0
      do while (keep_running)
         iter=iter+1
         x_mid=(x1+x2)/2.0D+0
         f_mid=fbeta(radius,h,x_mid)
         if (f1*f_mid.lt.0.0D+0) then
            x2=x_mid
            f2=f_mid
         else if (f_mid*f2.lt.0.0D+0) then
            x1=x_mid
            f1=f_mid
         else
            call error(label)
            write(*,*) 'x1=',x1,' f1=',f1
            write(*,*) 'x2=',x2,' f2=',f2
            write(*,*) 'x_mid=',x_mid,' f_mid=',f_mid
            write(*,*) 'f1*f_mid=',f1*f_mid
            write(*,*) 'f2*f_mid=',f2*f_mid
            write(*,*) 'one should be negative'
            stop
         endif
c     Debug
c         write(*,*) iter,beta1,beta2,dabs(beta2-beta1),dabs(f2-f1)
c     Debug
         if (dabs(x2-x1).lt.errx) then
            solution_found=.true.
            x0=x_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'x2-x1=',dabs(x2-x1),x0
c     Debug
         endif
         if (dabs(f_mid).lt.errf) then
            solution_found=.true.
            x0=x_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'f2-f1=',dabs(f2-f1),x0
c     Debug
         endif
         if (iter.gt.Niter_mx) then
            solution_found=.false.
            keep_running=.false.
         endif
      enddo                     ! while (keep_running)

      return
      end



      double precision function fbeta(radius,h,beta)
      implicit none
      include 'param.inc'
c      
c     Purpose: to evaluate the function of beta which roots
c     have to be found
c     
c     Input:
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c       + beta: value of beta where the function has to be evaluated
c    
c     Output:
c       + fbeta: value of the function for beta
c     
c     I/O
      double precision radius,h,beta
      
      fbeta=dtan(beta*radius)
     &     -(radius*beta)/(1.0D+0-h*radius)
      
      return
      end
      

      subroutine beta_discontinuity(Ndiscontinuity,radius,h,beta)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to compute the value of beta associated to a given discontinuity
c     
c     Input:
c       + Ndiscontinuity: index of the discontinuity
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c     
c     Output:
c       + beta: value of beta for the discontinuity
c     
c     I/O
      integer Ndiscontinuity
      double precision radius
      double precision h
      double precision beta
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine beta_discontinuity'

      beta=dble(Ndiscontinuity)*pi/(2.0D+0*radius)

      return
      end
      
