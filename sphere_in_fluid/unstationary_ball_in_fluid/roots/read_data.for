      subroutine read_data(data_file,dim,
     &     R,hc,hr,epsilon,rho_s,Cp_s,lambda_s,
     &     Text,Trad,Tinitial,
     &     bfactor,sfactor,Nterms,mc,
     &     Nevent,Nprobe,probe_positions,Ntime,time_positions)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c       + dim: dimension of space
c
c     Output:
c       + R: radius of the ball
c       + hc: convective exchange coefficient [W/(m^2.K)]
c       + hr: radiative exchange coefficient [W/(m^2.K)]
c       + epsilon: emissivity of the solid
c       + rho_s: density of the solid [kg/m^3]
c       + Cp_s: specific heat of the solid [J/(kg.K)]
c       + lambda_s: thermal conductivity of the solid [W/(m.K)]
c       + Text: temperature of the external fluid [K]
c       + Trad: external radiative exchange temperature [K]
c       + Tinitial: initial temperature of the solid [K]
c       + bfactor: delta_boundary=e/bfactor
c       + sfactor: delta_solid=e/sfactor
c       + Nterms: number of terms in the Fourier series expansion
c       + mc: true if MC computation has to be performed
c       + Nevent: number of statistical realizations
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c     
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      double precision R
      double precision hc
      double precision hr
      double precision epsilon
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision Text
      double precision Trad
      double precision Tinitial
      double precision bfactor
      double precision sfactor
      integer Nterms
      logical mc
      integer Nevent
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      character*(Nchar_mx) infile
c     temp
      integer i,j,ios
      character*(Nchar_mx) nlines_file
      double precision x(1:Nvec_mx)
      logical is_inside
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=data_file(1:strlen(data_file)),
     &     status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) data_file(1:strlen(data_file))
         stop
      else
         do i=1,6
            read(10,*)
         enddo ! i
         read(10,*) R
         read(10,*)
         read(10,*) hc
         read(10,*)
         read(10,*) hr
         read(10,*)
         read(10,*) epsilon
         read(10,*)
         read(10,*) rho_s
         read(10,*)
         read(10,*) Cp_s
         read(10,*)
         read(10,*) lambda_s
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Text
         read(10,*)
         read(10,*) Trad
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Tinitial
         do i=1,5
            read(10,*)
         enddo                  ! i
         read(10,*) bfactor
         read(10,*)
         read(10,*) sfactor
         read(10,*)
         read(10,*) Nterms
         read(10,*)
         read(10,*) mc
         do i=1,5
            read(10,*)
         enddo ! i
         read(10,*) Nevent
      endif
      close(10)
c     Read probe_positions
      infile='./data/probe_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Nprobe)
      if (Nprobe.gt.Np_mx) then
         call error(label)
         write(*,*) 'Nprobe=',Nprobe
         write(*,*) 'while Np_mx=',Np_mx
         stop
      endif
c      
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Nprobe
            read(11,*) (probe_positions(i,j),j=1,dim)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
c     
c     Read time_positions
      infile='./data/time_positions.in'
c     
      nlines_file='./nlines'
      call get_nlines(infile,nlines_file,Ntime)
      if (Ntime.gt.Nt_mx) then
         call error(label)
         write(*,*) 'Ntime=',Ntime
         write(*,*) 'while Nt_mx=',Nt_mx
         stop
      endif
c     
      open(11,file=infile(1:strlen(infile)),
     &     status='old',iostat=ios)
      if (ios.eq.0) then        ! file found
         do i=1,Ntime
            read(11,*) time_positions(i)
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Required file was not found:'
         write(*,*) infile(1:strlen(infile))
         stop
      endif
      close(11)
      
c     Inconsistencies
      if (R.le.0.0D+0) then
         call error(label)
         write(*,*) 'R=',R
         write(*,*) 'should be positive'
         stop
      endif
      if (hc.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hc=',hc
         write(*,*) 'should be positive'
         stop
      endif
      if (hr.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hr=',hr
         write(*,*) 'should be positive'
         stop
      endif
      if (epsilon.lt.0.0D+0) then
         call error(label)
         write(*,*) 'epsilon=',epsilon
         write(*,*) 'should be positive'
         stop
      endif
      if (rho_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'rho_s=',rho_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Cp_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Cp_s=',Cp_s
         write(*,*) 'should be positive'
         stop
      endif
      if (lambda_s.lt.0.0D+0) then
         call error(label)
         write(*,*) 'lambda_s=',lambda_s
         write(*,*) 'should be positive'
         stop
      endif
      if (Text.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Text=',Text
         write(*,*) 'should be positive'
         stop
      endif
      if (Tinitial.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Tinitial=',Tinitial
         write(*,*) 'should be positive'
         stop
      endif
      if (bfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'bfactor=',bfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (sfactor.lt.0.0D+0) then
         call error(label)
         write(*,*) 'sfactor=',sfactor
         write(*,*) 'should be positive'
         stop
      endif
      if (Nterms.lt.1) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'should be greater than 0'
         stop
      endif
      if (Nterms.gt.Nterms_mx) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'while Nterms_mx=',Nterms_mx
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
      do i=1,Nprobe
         do j=1,dim
            x(j)=probe_positions(i,j)
         enddo
         call x_is_in_volume(dim,x,is_inside)
         if (.not.is_inside) then
            call error(label)
            write(*,*) 'Probe position index:',i
            write(*,*) 'x=',(x(j),j=1,dim)
            write(*,*) 'is not in the volume of solid'
            stop
         endif
      enddo                     ! i
      do i=1,Ntime
         if (time_positions(i).lt.0.0D+0) then
            call error(label)
            write(*,*) 'Time index:',i
            write(*,*) 'time_positions(',i,')=',time_positions(i)
            write(*,*) '< 0'
            stop
         endif
      enddo                     ! i
            
      return
      end
