      subroutine analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to evaluate analytical temperature fields
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c       + Nterms: number of terms in the Fourier series expansion
c     
c     Output:
c       + Tunstationary: temperature profile at each (x/t) positions
c       + Tinit: initial temperature profile
c       + Tstationary: stationary (final) temperature profile
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Np_mx)
      integer Nterms
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     temp
      integer n
      integer itime,iprobe,iterm
      double precision time,probe_x(1:Nvec_mx)
      logical check,possible_root,keep_looking
      double precision max_err
      double precision Ts1,Ts2
      double precision x(1:Nvec_mx)
      double precision k
      double precision norm,norm_theoric
      double precision coeffa,coeffb
      double precision coeff(1:Nterms_mx)
      character*(Nchar_mx) str
      integer err
c     temp
      integer i,j
      integer Ndiscontinuity
      double precision betad
      logical debug
      double precision Tb1,Tb2,Ts_analytic
      double precision rad,theta,phi
      double precision radius,h
      double precision beta,beta1,beta2
      double precision as,bs
      double precision int_sin,int_cos
      double precision int_rsin,int_rcos
      double precision int_r2sin,int_r2cos
      double precision int_r3sin
      double precision f,f1,f2
      double precision errx,errf
      logical solution_found
      double precision beta0
      integer Np
      integer Nroots
      double precision root(1:Nterms_mx)
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
      logical keep_searching
      double precision dx
      integer Niter,Nfail
      double precision epsilon_theta
c     functions
      double precision fbeta
c     parameters
      double precision epsilon_theta0
      parameter(epsilon_theta0=1.0D-5) ! rad
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine analytical_results'

c     Initial temperature profile
      open(11,file='./results/Tsolid_initial.txt')
      write(11,10) 'Temperatures @ intial conditions'
      write(11,*)
      do i=1,Nprobe
         Ts_analytic=Tinitial
         write(11,*) probe_positions(i,1),Ts_analytic
         Tinit(i)=Ts_analytic
      enddo                     ! i
      close(11)
c     Stationary temperature profile
      open(11,file='./results/Tsolid_stationary.txt')
      write(11,10) 'Temperatures @ stationary'
      write(11,*)            
      do i=1,Nprobe
         Ts_analytic=P*(R**2.0D+0-probe_positions(i,1)**2.0D+0)
     &        /(6.0D+0*lambda_s)
     &        +(P*R/3.0D+0+hc*Text+hr*Trad)/(hc+hr)
         write(11,*) probe_positions(i,1),Ts_analytic
         Tstationary(i)=Ts_analytic
      enddo                     ! i
      close(11)

c     Debug
c      write(*,*) 'Solving roots...'
c     Debug
c     Solving T1, the unstationary homogeneous problem
      h=(hc+hr)/lambda_s
      radius=R
      f1=fbeta(radius,h,beta1)
      errx=1.0D-8
      errf=1.0D-8
      Nroots=0
      Nfail=0
      epsilon_theta=1.0D-10
c     First discontinuity: get betad
      Ndiscontinuity=1
      keep_looking=.true.
      do while (keep_looking)
         call beta_discontinuity(Ndiscontinuity,radius,h,betad)
         beta1=betad+epsilon_theta
         call beta_discontinuity(Ndiscontinuity+1,radius,h,betad)
         beta2=betad-epsilon_theta
         Ndiscontinuity=Ndiscontinuity+1
c     Debug
c         write(*,*) 'interval:',beta1,beta2,
c     &        fbeta(radius,h,beta1),fbeta(radius,h,beta2)
c     Debug
         call solve_fbeta_root(radius,h,beta1,beta2,errx,errf,
     &        solution_found,beta0)
         if (solution_found) then
            Nroots=Nroots+1
            if (Nroots.gt.Nterms_mx) then
               call error(label)
               write(*,*) 'Nroots=',Nroots
               write(*,*) '> Nterms_mx=',Nterms_mx
               stop
            else
               root(Nroots)=beta0
c     Debug
c               write(*,*) 'root(',Nroots,')=',root(Nroots)
c     Debug
               Nfail=0
c     Stop when enough terms have been found
               if (Nroots.ge.Nterms) then
                  keep_looking=.false.
               endif            ! Nroots > Nterms
            endif               ! Nroots > Nterms_mx
         else                   ! solution_found=.false
            Nfail=Nfail+1
c     Debug
c            write(*,*) 'Nfail=',Nfail
c     Debug
            if (Nfail.gt.10) then
               keep_looking=.false.
            endif
         endif                  ! solution_found         
      enddo                     ! while (keep_looking)
c
c     Generate log
      open(11,file='./results/roots_log.txt')
      write(11,*) 'Number of roots found:',Nroots
      if (Nroots.gt.0) then
         do i=1,Nroots
            write(11,*) 'root(',i,')=',root(i),fbeta(radius,h,root(i))
            k=h-1.0D+0/R
            norm_theoric=0.50D+0*(R*(root(i)**2.0D+0+k**2.0D+0)+k)
     &           /(root(i)**2.0D+0+k**2.0D+0)
            norm=R/2.0D+0-dsin(2.0D+0*root(i)*R)/(4.0D+0*root(i))
            write(11,*) 'norm_theoric=',norm_theoric,' norm=',norm
         enddo                  ! i
      endif
      close(11)
      
c     
c     Evaluation of the Fourier series coefficients
c      coeffa=Tinitial-(hc*Text+hr*Trad)/(hc+hr)
      do i=1,Nroots
         norm=R/2.0D+0-dsin(2.0D+0*root(i)*R)/(4.0D+0*root(i))
         int_sin=(1.0D+0-dcos(root(i)*R))/root(i)
         int_cos=dsin(root(i)*R)/root(i)
         int_rsin=-R*dcos(root(i)*R)/root(i)+int_cos/root(i)
         int_rcos=R*dsin(root(i)*R)/root(i)-int_sin/root(i)
         int_r2sin=-R**2.0D+0*dcos(root(i)*R)/root(i)
     &        +2.0D+0*int_rcos/root(i)
         int_r2cos=R**2.0D+0*dsin(root(i)*R)/root(i)
     &        -2.0D+0*int_rsin/root(i)
         int_r3sin=-R**3.0D+0*dcos(root(i)*R)/root(i)
     &        +3.0D+0*int_r2cos/root(i)
         coeff(i)=(P*int_r3sin/(6.0D+0*lambda_s)
     &        +(Tinitial-(P*R**2.0D+0)/(6.0D+0*lambda_s)
     &        -(P*R/3.0D+0+hc*Text+hr*Trad)/(hc+hr))*int_rsin)/norm
      enddo                     ! i
c     Generate log
      open(11,file='./results/coefficients_log.txt')
      do i=1,Nroots
         write(11,*) coeff(i)
      enddo                     ! i
      close(11)
c     
      do itime=1,Ntime
c     write(*,*) 'time index: ',itime,' /',Ntime
         time=time_positions(itime)
         do iprobe=1,Nprobe
            do j=1,dim
               probe_x(j)=probe_positions(iprobe,j)
            enddo               ! j
            call cart2spher(probe_x(1),probe_x(2),probe_x(3),
     &           rad,theta,phi)
            if (time.le.0.0D+0) then ! time = 0
               Ts_analytic=Tinitial
            else                ! time > 0
c     Initialization: solution of stationary problems #1 and #2
               Ts1=Tstationary(iprobe)
c     Fourier series evaluation
               Ts2=0.0D+0
               do iterm=1,Nroots
                  if ((root(iterm)*rad).lt.epsilon_theta0) then
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *root(iterm)
     &                    *(1.0D+0-((root(iterm)*rad)**2.0D+0)/6.0D+0) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
c     Debug
c                     write(*,*) coeff(iterm),root(iterm),Ts2
c     Debug
                  else
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *dsin(root(iterm)*rad) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
     &                    /rad  ! variable change u(r,t)=r.T(r,t)
c     Debug
c                     write(*,*) 'exp(-alphaT)=',
c     &                    dexp(-lambda_s/(rho_s*Cp_s)
c     &                    *(root(iterm)**2.0D+0)*time)
c     Debug
                  endif
               enddo            ! iterm
c     Debug
c               write(*,*) time,Ts2
c     Debug
               Ts_analytic=Ts1+Ts2
            endif               ! time = 0 or > 0
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
            call num2str(itime,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'itime=',itime
               stop
            else
               results_file_t='./results/Tsolid_analytical_t'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     + space files
            call num2str(iprobe,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'iprobe=',iprobe
               stop
            else
               results_file_t='./results/Tsolid_analytical_x'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     --------------------------------------------------------------------------------------
         enddo                  ! iprobe
         close(11)
      enddo                     ! itime
      
      return
      end



      subroutine solve_fbeta_root(radius,h,beta1,beta2,errx,errf,
     &     solution_found,x0)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a root of f(beta)=0 between two known boundaries
c     
c     Input:
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c       + m: index of the period
c       + beta1: lower beta boundary
c       + beta2: lower beta boundary
c       + errx: maximum error over x (beta)
c       + errf: maximum error over f
c    
c    Output:
c      + solution_found: true if a solution has been found
c      + x0: f(x0)=0
c    
c    I/O
      double precision radius,h
      double precision beta1,beta2
      double precision errx,errf
      logical solution_found
      double precision x0
c     temp
      double precision x1,x2,x_mid
      double precision f1,f2,f_mid
      integer iter
      logical keep_running
c     functions
      double precision fbeta
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine solve_fbeta_root'

      x1=beta1
      x2=beta2
      f1=fbeta(radius,h,x1)
      f2=fbeta(radius,h,x2)
      keep_running=.true.
      iter=0
      do while (keep_running)
         iter=iter+1
         x_mid=(x1+x2)/2.0D+0
         f_mid=fbeta(radius,h,x_mid)
         if (f1*f_mid.lt.0.0D+0) then
            x2=x_mid
            f2=f_mid
         else if (f_mid*f2.lt.0.0D+0) then
            x1=x_mid
            f1=f_mid
         else
            keep_running=.false.
            solution_found=.false.
            goto 123
         endif
c     Debug
c         write(*,*) iter,beta1,beta2,dabs(beta2-beta1),dabs(f2-f1)
c     Debug
         if (dabs(x2-x1).lt.errx) then
            solution_found=.true.
            x0=x_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'x2-x1=',dabs(x2-x1),x0
c     Debug
         endif
         if (dabs(f_mid).lt.errf) then
            solution_found=.true.
            x0=x_mid
            keep_running=.false.
c     Debug
c            write(*,*) 'f2-f1=',dabs(f2-f1),x0
c     Debug
         endif
         if (iter.gt.Niter_mx) then
            solution_found=.false.
            keep_running=.false.
         endif
 123     continue
      enddo                     ! while (keep_running)

      return
      end



      double precision function fbeta(radius,h,beta)
      implicit none
      include 'param.inc'
c      
c     Purpose: to evaluate the function of beta which roots
c     have to be found
c     
c     Input:
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c       + beta: value of beta where the function has to be evaluated
c    
c     Output:
c       + fbeta: value of the function for beta
c     
c     I/O
      double precision radius,h,beta
      
      fbeta=dtan(beta*radius)
     &     -(radius*beta)/(1.0D+0-h*radius)
      
      return
      end
      

      subroutine beta_discontinuity(Ndiscontinuity,radius,h,beta)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to compute the value of beta associated to a given discontinuity
c     
c     Input:
c       + Ndiscontinuity: index of the discontinuity
c       + radius: radius of the sphere [m]
c       + h: total exchange coefficient
c     
c     Output:
c       + beta: value of beta for the discontinuity
c     
c     I/O
      integer Ndiscontinuity
      double precision radius
      double precision h
      double precision beta
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine beta_discontinuity'

      beta=dble(Ndiscontinuity)*pi/(2.0D+0*radius)

      return
      end
      
