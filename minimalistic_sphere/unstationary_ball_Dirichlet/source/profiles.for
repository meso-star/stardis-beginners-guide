      subroutine initial_solid_temperature(dim,x,Tinit)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to get the initial temperature at a given position
c     
c     Input:
c       + dim: dimension of space
c       + x: position where the initial temperature must be evaluated
c     
c     Output:
c       + Tinit: initial temperature @ x
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      double precision Tinit
c     temp
      double precision t(1:Nvec_mx)
      double precision d,nf
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine initial_solid_temperature'

      Tinit=Tinitial
c     Debug
c      write(*,*) 'Initial condition was reached'
c     Debug

      return
      end
