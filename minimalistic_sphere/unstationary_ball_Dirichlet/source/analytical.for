      subroutine analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
      include 'param.inc'
      include 'formats.inc'
c     
c     Purpose: to evaluate analytical temperature fields
c     
c     Input:
c       + dim: dimension of space
c       + Nprobe: number of probe positions
c       + probe_positions: probe positions
c       + Ntime: number of time points
c       + time_positions: temporal positions
c       + Nterms: number of terms in the Fourier series expansion
c     
c     Output:
c       + Tunstationary: temperature profile at each (x/t) positions
c       + Tinit: initial temperature profile
c       + Tstationary: stationary (final) temperature profile
c     
c     I/O
      integer dim
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Np_mx)
      integer Nterms
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     temp
      integer n
      integer itime,iprobe,iterm
      double precision time,probe_x(1:Nvec_mx)
      logical check,possible_root,keep_looking
      double precision max_err
      double precision Ts1,Ts2
      double precision x(1:Nvec_mx)
      double precision k
      double precision norm,norm_theoric
      double precision coeffa,coeffb
      double precision coeff(1:Nterms_mx)
      character*(Nchar_mx) str
      integer err
c     temp
      integer i,j
      integer Ndiscontinuity
      double precision betad
      logical debug
      double precision Tb1,Tb2,Ts_analytic
      double precision rad,theta,phi
      double precision radius,h
      double precision beta,beta1,beta2
      double precision as,bs
      double precision int_sin,int_cos
      double precision int_rsin,int_rcos
      double precision int_r2sin,int_r2cos
      double precision int_r3sin
      double precision f,f1,f2
      double precision errx,errf
      logical solution_found
      double precision beta0
      integer Np
      integer Nroots
      double precision root(1:Nterms_mx)
      character*(Nchar_mx) results_file_t
      character*(Nchar_mx) results_file_x
      logical keep_searching
      double precision dx
      integer Niter,Nfail
      double precision epsilon_theta
      logical is_nan
c     functions
      double precision fbeta
c     parameters
      double precision epsilon_theta0
      parameter(epsilon_theta0=1.0D-5) ! rad
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine analytical_results'

c     Initial temperature profile
      open(11,file='./results/Tsolid_initial.txt')
      write(11,10) 'Temperatures @ intial conditions'
      write(11,*)
      do i=1,Nprobe
         Ts_analytic=Tinitial
         write(11,*) probe_positions(i,1),Ts_analytic
         Tinit(i)=Ts_analytic
      enddo                     ! i
      close(11)
c     Stationary temperature profile
      open(11,file='./results/Tsolid_stationary.txt')
      write(11,10) 'Temperatures @ stationary'
      write(11,*)            
      do i=1,Nprobe
         Ts_analytic=P*(R**2.0D+0-probe_positions(i,1)**2.0D+0)
     &        /(6.0D+0*lambda_s)
     &        +Tboundary
         write(11,*) probe_positions(i,1),Ts_analytic
         Tstationary(i)=Ts_analytic
      enddo                     ! i
      close(11)

c     Debug
c      write(*,*) 'Solving roots...'
c     Debug
c     Solving T1, the unstationary homogeneous problem
      radius=R
      keep_looking=.true.
      do iterm=1,Nterms
         root(iterm)=iterm*pi/R
c     Debug
         call test_nan(root(iterm),is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'root(',iterm,')=',root(iterm)
            stop
         endif
c     Debug
      enddo                     ! iterm
      Nroots=Nterms
c     Generate log
      open(11,file='./results/roots_log.txt')
      write(11,*) 'Number of roots found:',Nroots
      if (Nroots.gt.0) then
         do i=1,Nroots
            write(11,*) 'root(',i,')=',root(i)
         enddo                  ! i
      endif
      close(11)
 
c     
c     Evaluation of the Fourier series coefficients
      do i=1,Nroots
         norm=R/2.0D+0
         int_sin=(1.0D+0-dcos(root(i)*R))/root(i)
         int_cos=dsin(root(i)*R)/root(i)
         int_rsin=-R*dcos(root(i)*R)/root(i)+int_cos/root(i)
         int_rcos=R*dsin(root(i)*R)/root(i)-int_sin/root(i)
         int_r2sin=-R**2.0D+0*dcos(root(i)*R)/root(i)
     &        +2.0D+0*int_rcos/root(i)
         int_r2cos=R**2.0D+0*dsin(root(i)*R)/root(i)
     &        -2.0D+0*int_rsin/root(i)
         int_r3sin=-R**3.0D+0*dcos(root(i)*R)/root(i)
     &        +3.0D+0*int_r2cos/root(i)
         coeff(i)=(P*int_r3sin/(6.0D+0*lambda_s)
     &        +(Tinitial-(P*R**2.0D+0)/(6.0D+0*lambda_s)
     &        -Tboundary)*int_rsin)/norm
c         coeff(i)=2.0D+0*(Tboundary-Tinitial)*dcos(root(i)*R)/root(i)
c     Debug
         call test_nan(coeff(i),is_nan)
         if (is_nan) then
            call error(label)
            write(*,*) 'coeff(',i,')=',coeff(i)
            write(*,*) 'root(',i,')=',root(i)
            stop
         endif
c     Debug
      enddo                     ! i
c     Generate log
      open(11,file='./results/coefficients_log.txt')
      do i=1,Nroots
         write(11,*) coeff(i)
      enddo                     ! i
      close(11)
c     
      do itime=1,Ntime
c     write(*,*) 'time index: ',itime,' /',Ntime
         time=time_positions(itime)
         do iprobe=1,Nprobe
            do j=1,dim
               probe_x(j)=probe_positions(iprobe,j)
            enddo               ! j
            call cart2spher(probe_x(1),probe_x(2),probe_x(3),
     &           rad,theta,phi)
            if (time.le.0.0D+0) then ! time = 0
               Ts_analytic=Tinitial
            else                ! time > 0
c     Initialization: solution of stationary problems #1 and #2
               Ts1=Tstationary(iprobe)
c     Fourier series evaluation
               Ts2=0.0D+0
               do iterm=1,Nroots
                  if ((root(iterm)*rad).lt.epsilon_theta0) then
c                     Ts2=Ts2
c     &                    +2.0D+0*(Tboundary-Tinitial)
c     &                    *(-1.0D+0)**(dble(iterm))
c     &                    *dexp(-lambda_s/(rho_s*Cp_s)
c     &                    *(root(iterm)**2.0D+0)*time) ! function of time
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *root(iterm)
     &                    *(1.0D+0-((root(iterm)*rad)**2.0D+0)/6.0D+0) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
                  else
c                     Ts2=Ts2
c     &                    +2.0D+0*(Tboundary-Tinitial)*R/(pi*rad)
c     &                    *(-1.0D+0)**(dble(iterm))/dble(iterm)
c     &                    *dsin(iterm*pi*rad/R) ! function of space
c     &                    *dexp(-lambda_s/(rho_s*Cp_s)
c     &                    *(root(iterm)**2.0D+0)*time) ! function of time
                     Ts2=Ts2
     &                    +coeff(iterm) ! Fourier coefficient
     &                    *dsin(root(iterm)*rad) ! function of space
     &                    *dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time) ! function of time
     &                    /rad  ! variable change u(r,t)=r.T(r,t)
                  endif
c     Debug
                  call test_nan(Ts2,is_nan)
                  if (is_nan) then
                     call error(label)
                     write(*,*) 'Ts2=',Ts2
                     write(*,*) 'coeff(',iterm,')=',coeff(iterm)
                     write(*,*) 'root(',iterm,')=',root(iterm)
                     write(*,*) 'rad=',rad
                     write(*,*) 'time=',time
                     write(*,*) 'sin(rad*root)=',dsin(root(iterm)*rad)
                     write(*,*) 'exp=',dexp(-lambda_s/(rho_s*Cp_s)
     &                    *(root(iterm)**2.0D+0)*time)
                     stop
                  endif
c     Debug
               enddo            ! iterm
c     Debug
c               write(*,*) time,Ts2
c     Debug
               Ts_analytic=Ts1+Ts2
            endif               ! time = 0 or > 0
c     --------------------------------------------------------------------------------------
c     Record results:
c     + time files
            call num2str(itime,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'itime=',itime
               stop
            else
               results_file_t='./results/Tsolid_analytical_t'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     + space files
            call num2str(iprobe,str,err)
            if (err.eq.1) then
               call error(label)
               write(*,*) 'Could not convert to string:'
               write(*,*) 'iprobe=',iprobe
               stop
            else
               results_file_t='./results/Tsolid_analytical_x'
     &              //str(1:strlen(str))
     &              //'.txt'
            endif
            open(11,file=results_file_t(1:strlen(results_file_t))
     &           ,access='append')
            write(11,*) probe_x,time,Ts_analytic
            close(11)
c     --------------------------------------------------------------------------------------
         enddo                  ! iprobe
         close(11)
      enddo                     ! itime
      
      return
      end
