c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine add_slash_if_absent(st)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a slash character '/' at the end of a character string
c     when there is none.
c
c     I/O:
c       + st: character string
c

c     I/O
      character*(Nchar_mx) st
c     temp
      character*1 sstring
      integer n
      integer strlen
      character*(Nchar_mx) label
      label='subroutine add_slash_if_absent'

      sstring='/'

      n=strlen(st)
      if (st(n:n).ne.sstring(1:1)) then
         st=st(1:strlen(st))//sstring(1:1)
      endif

      return
      end
