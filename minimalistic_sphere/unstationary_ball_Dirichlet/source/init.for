      subroutine init(mc)
      implicit none
      include 'max.inc'
c
c     Purpose: initialization tasks
c
c     Input:
c       + mc: true if MC computation has to be performed
c
c     I/O
      logical mc
c     temp
      character*(Nchar_mx) command
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_data'

      if (mc) then
         command='rm -rf ./results/Tsolid_t*'
         call exec(command)
         command='rm -rf ./results/Tsolid_x*'
         call exec(command)
      endif
      command='rm -rf ./results/Tsolid_analytical_t*'
      call exec(command)
      command='rm -rf ./results/Tsolid_analytical_x*'
      call exec(command)
      
      return
      end
      
