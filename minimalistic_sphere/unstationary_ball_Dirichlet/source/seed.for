      subroutine read_seed(seed)
      implicit none
      include 'max.inc'
c     I/O
      integer seed
c     temp
      integer ios
      character*(Nchar_mx) seedfile
      character*(Nchar_mx) oldseedfile
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine read_seed'

      seedfile='./config/seed'
      open(11,file=seedfile(1:strlen(seedfile))
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then ! file not found
         call error(label)
         write(*,*) 'file could not be found:'
         write(*,*) seedfile(1:strlen(seedfile))
         stop
      endif
      read(11,*) seed
      close(11)

      oldseedfile='./config/old_seed'
      open(12,file=oldseedfile(1:strlen(oldseedfile)))
      write(12,*) seed
      close(12)

      return
      end



      subroutine write_seed()
      implicit none
      include 'max.inc'
c     temp
      integer seed
      character*(Nchar_mx) seedfile
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine write_weed'

      seedfile='./config/seed'

      call generate_1seed(seed)

      open(11,file=seedfile(1:strlen(seedfile)))
      write(11,*) seed
      close(11)

      return
      end



      subroutine generate_1seed(seed)
      implicit none
      include 'max.inc'
c     I/O
      integer seed
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine generate_1seed'
c
c     Purpose: to generate a seed that "zufalli" will use
c     to initialize the random number generator "zufall"
c
c     The seed must be 0 < seed < 31328
c
      call random_gen(R)
c     Debug
c      if (R.gt.1.0D+0) then
c         call error(label)
c         write(*,*) 'R=',R
c         stop
c      endif
c     Debug
      seed=int(R*31329)

      return
      end
