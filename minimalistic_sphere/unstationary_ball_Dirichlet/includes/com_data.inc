      double precision R
      double precision rho_s
      double precision Cp_s
      double precision lambda_s
      double precision P
      double precision Tboundary
      double precision Tinitial
      double precision bfactor
      double precision sfactor
      integer Nevent
      integer seeds(1:Nproc_mx)

      common /com_data1/ R
      common /com_data2/ rho_s
      common /com_data3/ Cp_s
      common /com_data4/ lambda_s
      common /com_data5/ P
      common /com_data6/ Tboundary
      common /com_data7/ Tinitial
      common /com_data8/ bfactor
      common /com_data9/ sfactor
      common /com_data10/ Nevent
      common /com_data11/ seeds
