      subroutine distance_cart(x1,y1,z1,x2,y2,z2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their cartesian coordinates
c
c     inputs
      double precision x1,y1,z1
      double precision x2,y2,z2
c     outputs
      double precision d
c     label
      character*(Nchar_mx) label
      label='subroutine distance_cart'

      d=dsqrt((x2-x1)**2.0D+0+(y2-y1)**2.0D+0+(z2-z1)**2.0D+0)

      return
      end


      
      subroutine x_is_in_volume(dim,x,is_inside)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to check whether of not a position is in the volume of solid
c     
c     Input:
c       + dim: dimension of space
c       + x: position to check [cartesian coordinates]
c     
c     Output:
c       + is_inside: true if "x" is inside; false otherwise
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      logical is_inside
c     temp
      double precision d
c     parameters
      double precision epsilon_r
      parameter(epsilon_r=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='subroutine x_is_in_volume'

      is_inside=.true.
      call distance_cart(0.0D+0,0.0D+0,0.0D+0,x(1),x(2),x(3),d)
      if (d.gt.R+epsilon_r) then
         is_inside=.false.
      endif
      
      return
      end
      

      subroutine x_is_on_boundary(dim,x,is_on_boundary,boundary_index)
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to check whether of not a position is over the boundary of the volume of solid
c     
c     Input:
c       + dim: dimension of space
c       + x: position to check
c     
c     Output:
c       + is_on_boundary: true if "x" is on the boundary; false otherwise
c       + boundary_index: index of boundary if is_on_boundary=T
c     
c     I/O
      integer dim
      double precision x(1:Nvec_mx)
      logical is_on_boundary
      integer boundary_index
c     temp
      integer i,j
      double precision d
c     parameters
      double precision epsilon_r
      parameter(epsilon_r=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='subroutine x_is_on_boundary'

      is_on_boundary=.false.
      call distance_cart(0.0D+0,0.0D+0,0.0D+0,x(1),x(2),x(3),d)
      if (dabs(d-R).lt.epsilon_r) then
         is_on_boundary=.true.
         boundary_index=1
      endif
      
      return
      end

      

      subroutine cart2spher(x,y,z,r,theta,phi)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to convert (x,y,z) cartesian coordinates
c     into (r,theta,phi) spherical coordinates
c
c     Inputs:
c       + x (m)
c       + y (m)
c       + z (m)
c
c     Outputs:
c       + r: radius (m), or altitude relative to the center of the planet
c       + theta: latitude (rad) [-pi/2,pi/2]
c       + phi: longitude rad) [0:2*pi]
c
c     I/O
      double precision x,y,z
      double precision r,theta,phi
c     temp
      double precision cosp,sinp,cost,sint
      double precision a,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine cart2spher'

      a=dsqrt(x**2.0D+0+y**2.0D+0)
      r=dsqrt(x**2.0D+0+y**2.0D+0+z**2.0D+0)
      if (a.eq.0.0D+0) then
         cosp=1.0D+0
         sinp=0.0D+0
      else
         cosp=x/a
         sinp=y/a
      endif
      cost=a/r
      sint=z/r
      
      t1=dacos(dabs(cosp))
      if (x.ge.0.0D+0) then
         if (sinp.ge.0.0D+0) then
            phi=t1
         else
            phi=2.0D+0*pi-t1
         endif
      else
         if (sinp.ge.0.0D+0) then
            phi=pi-t1
         else
            phi=pi+t1
         endif
      endif

      t2=dacos(cost)
      if (sint.ge.0.0D+0) then
         theta=t2
      else
         theta=-t2
      endif
      
      return
      end
