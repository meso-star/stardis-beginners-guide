      program unstationary
      implicit none
      include 'max.inc'
      include 'com_data.inc'
c     
c     Purpose: to compute a set probe temperature T(x,t)
c     in a solid ball wuth known initial temperature and source term, 
c     with a Dirichlet limit condition (known surface temperature)
c     
c     Variables
      integer dim
      character*(Nchar_mx) data_file
      integer Nterms
      integer Nprobe
      double precision probe_positions(1:Np_mx,1:Nvec_mx)
      integer Ntime
      double precision time_positions(1:Nt_mx)
      double precision Tunstationary(1:Np_mx,1:Nt_mx)
      double precision Tinit(1:Np_mx)
      double precision Tstationary(1:Np_mx)
c     Label
      character*(Nchar_mx) label
      label='program unstationary'

      dim=3                     ! dimension of space
      data_file='./data.in'
      call read_data(data_file,dim,
     &     R,rho_s,Cp_s,lambda_s,P,
     &     Tboundary,Tinitial,
     &     Nterms,Nprobe,probe_positions,Ntime,time_positions)

      call init()
      
      call analytical_results(dim,Nprobe,probe_positions,
     &     Ntime,time_positions,Nterms,
     &     Tunstationary,Tinit,Tstationary)

      end
