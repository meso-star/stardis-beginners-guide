Calcul du profil de température spatio/temporel dans une boule de rayon R:

- conditions aux limites: Dirichlet (température de paroi imposée)

- condition initiale: profil uniforme

Fichiers d'entrée:

1- data.in: propriétés thermophysiques, terme source volumique, condition initiale
et aux limites, paramètres numériques.

2- probe_positions.in: positions sonde; par ligne: x / y / z

3- time_positions.in: temps sonde; par ligne: t


Fichiers de sortie:

1- Tsolid_analytical_t*.txt: températures sonde, pour chaque valeur du temps sonde;
par ligne:
x / y / z / t / T(x,t)

1- Tsolid_analytical_x*.txt: températures sonde, pour chaque valeur de la position sonde;
par ligne:
x / y / z / t / T(x,t)
